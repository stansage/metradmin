// Console.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include "Ear.hpp"
#include "Service.hpp"
#include "../Adapter/interface.h"

#undef HELP_KEY

namespace
{
	static const std::string HELP_KEY = "help";
	static const std::string CONFIG_KEY = "config";
	static const std::string DEBUG_KEY = "debug";
	static const std::string PORT_KEY = "port";
	static const std::string LISTEN_KEY = "listen";
	static const std::string REFRESH_KEY = "refresh";

	static const auto PORT_VALUE = 2115;
	static const auto LISTEN_VALUE = 7007;
	static const auto REFRESH_VALUE = 5;

	BOOL WINAPI ctrlHandler(DWORD type)
	{
		switch (type) {
		case CTRL_C_EVENT:
			return TRUE;
		}
		return FALSE;
	}
}

int _tmain(int argc, _TCHAR * argv[])
{
	namespace po = boost::program_options;
	namespace fs = boost::filesystem;

	fs::path path = fs::initial_path<fs::path>();
	path = fs::system_complete(fs::path(argv[0]));

	if (SetConsoleCtrlHandler(ctrlHandler, TRUE) == FALSE) {
		std::clog << "cannot hook Control-C" << std::endl;
	}

	po::options_description options;
	options.add_options()
		(HELP_KEY.c_str(), "produce help message")
		(CONFIG_KEY.c_str(), po::value<std::string>(), "use json config");

	po::variables_map variables;
	try {
		auto parser = po::parse_command_line(argc, argv, options);
		po::store(parser, variables);
	} catch (const std::exception & exception) {
		std::cerr << exception.what() << std::endl;
		return 1;
	}

	if (variables.count(HELP_KEY) != 0) {
		std::cout << options << std::endl;
		return 0;
	}

	nlohmann::json settings;
	try {
		if (fs::exists(path.replace_extension('.' + CONFIG_KEY))) {
			std::ifstream file(path.native());
			if (!file.is_open()) {
				throw std::runtime_error("Failed to read " + path.string());
			}
			settings = nlohmann::json::parse(file);
		} else {
			po::notify(variables);
			
			BOOST_ASSERT(variables.count(CONFIG_KEY) != 0);
			auto value = variables[CONFIG_KEY].as<std::string>();
			std::ifstream file(value);
			
			if (file.is_open()) {
				settings = nlohmann::json::parse(file);
			} else {
				std::replace(std::begin(value), std::end(value), '\'', '"');
				settings = nlohmann::json::parse(value);
			}
		}
	} catch (const std::exception & exception) {
		std::cerr << exception.what() << std::endl;
		return 1;
	}

	const auto debug = settings.find(DEBUG_KEY) != settings.end() && settings[DEBUG_KEY];

	auto port = 0;
	if (!debug) {
		const auto it = settings.find(PORT_KEY);
		port = (it != settings.end()) ? it.value() : PORT_VALUE;
	}

	const auto adapter = AttachAdapter(-port);
	if (debug) {
		std::clog << "working in debug mode" << std::endl;
		for (auto data = -1; data != 0; data = ReadAdapter(adapter)) {
			if (data != -1) {
				std::clog << NanoTime() << " adapter: " << adapter << " - " << data << std::endl;
			}
		}
	} else {
		const auto it = settings.find(LISTEN_KEY);
		port = (it != settings.end()) ? it.value() : LISTEN_VALUE;

		if (settings.find(REFRESH_KEY) == settings.end()) {
			settings[REFRESH_KEY] = REFRESH_VALUE;
		}

		Service service(settings);
		Ear ear(service, port);

		ear.listen();

		for (std::string command; !std::cin.eof(); command.clear()) {
			std::cout << "enter command: ";
			std::cin >> command;
			if (command.compare("b") == 0) {
				WriteAdapter(adapter, 2);
			}
			else if (command.compare("s") == 0) {
				WriteAdapter(adapter, 3);
			}
			else if (command.compare("q") == 0) {
				WriteAdapter(adapter, 0);
			}
			else {
				break;
			}
		}

		ear.abort();
	}

	DetachAdapter(adapter);

	return 0;
}

