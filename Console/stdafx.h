// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"
#include "../json/json.hpp"

#include <tchar.h>

#include <atomic>
#include <cstdint>
#include <string>
#include <iostream>
#include <mutex>
#include <fstream>
#include <thread>
#include <condition_variable>
#include <unordered_map>

#include <boost/format.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/core/noncopyable.hpp>
#include <boost/system/config.hpp>
#include <boost/asio.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/interprocess/permissions.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/interprocess/windows_shared_memory.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/thread/locks.hpp> 
#include <boost/filesystem.hpp>