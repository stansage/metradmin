#include "stdafx.h"
#include "Ear.hpp"
#include "Service.hpp"
#include "Observer.hpp"

struct Ear::Channel : boost::noncopyable
{
	using Data = Terminal::RawData;
	using Buffer = boost::shared_ptr<boost::asio::streambuf>;

	Ear & ear;
	Ear::Tcp::socket socket;
	boost::shared_ptr<std::thread> thread;

	Channel(Ear & owner) :
		ear(owner),
		socket(ear.ios_)
	{
	}

	void fail(const boost::system::error_code & error, const Data & data)
	{
		switch (error.value()) {
		case 2: // end of file
		case WSAECONNRESET:
			socket.close();
			break;
		default:
			std::cerr << "Incomplete request: " << error.message() << " (" << error.value() << ") - " << socket.is_open() << std::endl;
			std::copy(std::begin(data), std::end(data), std::ostreambuf_iterator<Data::value_type>(std::cerr));
			std::cerr << std::endl;
		}
	}

	void open()
	{
		using namespace boost::asio::placeholders;
		auto buffer = boost::make_shared<boost::asio::streambuf>();
		boost::asio::async_read_until(socket, *buffer, '\x04', boost::bind(&Channel::receive, this, buffer, error, bytes_transferred));
	}

	void receive(Buffer buffer, boost::system::error_code error, std::size_t size)
	{
		Data data = { buffers_begin(buffer->data()), buffers_end(buffer->data()) };
		buffer->consume(size);

		if (error) {
			fail(error, data);
		} else {
			const std::string header = { &data[0], size - 1 };
			Observer * observer = nullptr;

			if (header.front() == '#') {
				observer = ear.service_.restart(header.substr(1));
			} else {
				size = boost::asio::read_until(socket, *buffer, '\0', error);

				if (error) {
					fail(error, data);
				} else {
					data.assign(buffers_begin(buffer->data()), buffers_end(buffer->data()));
					buffer->consume(size);

					BOOST_ASSERT(data.size() >= size);

					const std::string count = { &data[0], size - 1 };
					const auto length = std::stoi(count);

					if (length > 0) {
						size = boost::asio::read(socket, *buffer, boost::asio::transfer_exactly(length - data.size() + size), error);

						if (error) {
							fail(error, data);
						} else if (size > 0) {
							data.assign(buffers_begin(buffer->data()), buffers_end(buffer->data()));
							buffer->consume(size);

							BOOST_ASSERT(data.size() == length);
							observer = ear.service_.start(header, data);
						}
					}
				}
			}

			if (observer != nullptr && !observer->active() && observer->start()) {
				observer->watch(std::bind(&Channel::send, this, std::placeholders::_1));
			}
		}

		if (socket.is_open()) {
			open();
		} else {
			ear.service_.shutdown();
			ear.accept();
		}
	}

	void send(std::string data)
	{
		if (socket.is_open()) {
			boost::system::error_code error;
			boost::asio::write(socket, boost::asio::buffer(data), error);
			if (error) {
				fail(error, Data(data.begin(), data.end()));
			}
		}
		if (!socket.is_open()) {
			ear.service_.shutdown();
		}
	}
};

Ear::Ear(Service & service, int port) :
service_(service),
acceptor_(ios_, Tcp::endpoint(Tcp::v4(), port))
{
}

Ear::~Ear()
{
	abort();
}

void Ear::listen()
{
	channel_ = std::make_shared<Channel>(*this);
	channel_->thread.reset(new std::thread([this]{
		accept();
		return ios_.run();
	}));
}

void Ear::abort()
{
	if (channel_->socket.is_open()) {
		boost::system::error_code error;
		channel_->socket.shutdown(boost::asio::ip::tcp::socket::shutdown_receive, error);
		channel_->socket.close(error);
	}
	if (!ios_.stopped()) {
		ios_.stop();
		channel_->thread->join();
	}
}

void Ear::accept()
{
	acceptor_.async_accept(channel_->socket, [this](const boost::system::error_code & error) {
		if (error) {
			std::cerr << "Failed to listen " << error.message() << " (" << error.value() << ")" << std::endl; 
		} else {
			channel_->open();
		}
	});
}
