#pragma once

#include "Terminal.hpp"

class Service : private boost::noncopyable
{
public:
	explicit Service(nlohmann::json & config);
	
	int refreshTimeout() const;

	Observer * start(const std::string & header, const Terminal::RawData & data);
	Observer * restart(const std::string & uuid);

	void shutdown();


private:
	nlohmann::json config_;
	std::map<std::string, std::shared_ptr<Terminal> > terminals_;
};

