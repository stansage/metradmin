#pragma once

class Observer;
class Service;

class Terminal : private boost::noncopyable
{
public:
	using RawData = std::vector<char>;
	explicit Terminal(Service & service, const nlohmann::json & config, const RawData & srvfile);

	Observer * observer() const;

	void update(const nlohmann::json & config, const RawData & srvfile);

private:
	HANDLE process_ = NULL;
	std::string template_;
	boost::filesystem::path root_;
	boost::filesystem::path ini_;
	boost::filesystem::path preset_;
	boost::filesystem::path info_;
	std::shared_ptr<Observer> observer_;
	nlohmann::json last_;
};



