#pragma once

class Service;

class Ear : private boost::noncopyable
{
public:
	explicit Ear(Service & service, int port);
	~Ear();

	void listen();
	void abort();

private:
	void accept();

private:
	friend struct Channel;
	struct Channel;
	using Tcp = boost::asio::ip::tcp;

	Service & service_;
	boost::asio::io_service ios_;
	Tcp::acceptor acceptor_;
	std::shared_ptr<Channel> channel_;
};

