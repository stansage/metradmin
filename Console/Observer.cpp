#include "stdafx.h"
#include "Observer.hpp"

namespace
{
	static const auto TIMEOUT = 300;
	static const auto THRESHOLD = 100;
}

Observer::Observer(const std::string & uuid, const Path & info, const String & cmdline, const String & cwd) :
uuid_(uuid),
info_(info),
cmdline_(cmdline),
cwd_(cwd)
{
	stopped_ = false;
	reset();
}

Observer::~Observer()
{
	abort();
	if (const auto p = previous_.load()) {
		delete p;
	}
}

bool Observer::active() const
{
	return thread_ && !stopped_;
}

void Observer::watch(Callback callback)
{
	callback_ = callback;
	if (thread_ && thread_->joinable()) {
		thread_->join();
	}
	thread_ = std::make_shared<std::thread>(std::bind(&Observer::run, this));
}

void Observer::abort()
{
	stopped_ = true;
	if (handle_ != NULL) {
		auto code = 0ul;
		if (GetExitCodeProcess(handle_, &code) && code == STILL_ACTIVE) {
			TerminateProcess(handle_, 0);
		}
		CloseHandle(handle_);
		handle_ = NULL;
	}
	if (thread_->get_id() != std::this_thread::get_id()) {
		thread_->join();
	}
}

void Observer::reset()
{
	if (const auto p = previous_.load()) {
		delete p;
	}
	previous_.store(new std::string());
}

bool Observer::start()
{
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	if (CreateProcess(NULL, &cmdline_[0], NULL, NULL, FALSE, 0, NULL, &cwd_[0], &si, &pi) != TRUE) {
		boost::system::error_code error(GetLastError(), boost::system::get_system_category());
		std::clog << "Failed to create process " << error.message() << " (" << error.value() << ")" << std::endl;
		return false;
	}

	stopped_ = false;
	handle_ = pi.hProcess;
	return true;
}

void Observer::run()
{
	namespace fs = boost::filesystem;
	nlohmann::json json;
	auto threshold = THRESHOLD;
	
	while (!stopped_) {
		boost::system::error_code error;
		if (fs::exists(info_, error) && !error) {
			threshold = -1;

			auto path = info_.parent_path() /= fs::unique_path();
			fs::rename(info_, path, error);
			if (error) {
				path = info_;
			}

			std::string line;
			std::ifstream file(path.native());
			BOOST_ASSERT(file.is_open());
				
			if (std::getline(file, line)) {
				const auto id = std::stoul(line);
				const auto access = PROCESS_QUERY_INFORMATION | SYNCHRONIZE | PROCESS_TERMINATE;
				const auto handle = OpenProcess(access, FALSE, id);
				if (handle != handle_) {
					CloseHandle(handle_);
					handle_ = handle;
				}
			}
			if (std::getline(file, line) && previous_.load()->compare(line) != 0) {
				delete previous_.load();
				previous_.store(new std::string(line));
				post(json, line);
			}
			file.close();
			boost::filesystem::remove(path, error);

		} else if (threshold == 0) {
			break;
		} else if (threshold > 0) {
			--threshold;
		}

		BOOST_ASSERT(handle_ != NULL);
		if (threshold > 0) {
			std::this_thread::sleep_for(std::chrono::milliseconds(TIMEOUT));
		} else if (WaitForSingleObject(handle_, TIMEOUT) == WAIT_OBJECT_0) {
			post(json, std::string());
			break;
		}
	}
	stopped_ = true;
}


void Observer::post(nlohmann::json & json, const std::string & data)
{
	json.clear();
	json["uuid"] = uuid_;

	const auto pos = data.find(';');
	if (pos != std::string::npos) {
		json["balance"] = data.substr(0, pos);
		json["currency"] = data.substr(pos + 1);
	}
	callback_(json.dump());
}