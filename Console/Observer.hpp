#pragma once
class Observer
{
	using Callback = std::function<void(std::string)>;
	using Path = boost::filesystem::path;

public:
	using String = std::vector<TCHAR>;

	explicit Observer(const std::string & uuid, const Path & info, const String & cmdline, const String & cwd);
	~Observer();

	bool active() const;
	void watch(Callback callback);
	void abort();
	void reset();
	bool start();

private:
	void run();
	void post(nlohmann::json & json, const std::string & data);

private:
	HANDLE handle_ = NULL;
	std::string uuid_;
	Path info_;
	String cmdline_;
	String cwd_;
	Callback callback_;
	std::shared_ptr<std::thread> thread_;
	std::atomic_bool stopped_;
	std::atomic<std::string*> previous_;
};

