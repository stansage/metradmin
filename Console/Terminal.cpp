#include "stdafx.h"
#include "Terminal.hpp"
#include "Observer.hpp"
#include "Service.hpp"

namespace fs = boost::filesystem;

namespace
{
	static const auto INI_NAME = "start.ini";
	static const auto CONFIG_NAME = "config";
	static const auto MQL_NAME = "MQL4";
	static const auto PRESETS_NAME = "Presets";
	static const auto LIBRARIES_NAME = "Libraries";
	static const auto FILES_NAME = "Files";
	static const auto PRESET_NAME = "parameters.set";
	static const auto TERMINAL_OPTIONS = " /portable ";

	static bool copyDirectory(fs::path const & source, fs::path const & destination)
	{
		try {
			// Check whether the function call is valid
			if (!fs::exists(source) || !fs::is_directory(source)) {
				std::cerr << "Source directory " << source.string() << " does not exist or is not a directory." << std::endl;
				return false;
			}
			if (fs::exists(destination)) {
				std::cerr << "Destination directory " << destination.string() << " already exists." << std::endl;
				return false;
			}
			// Create the destination directory
			if (!fs::create_directory(destination)) {
				std::cerr << "Unable to create destination directory" << destination.string() << std::endl;
				return false;
			}
		} catch (fs::filesystem_error const & e) {
			std::cerr << e.what() << std::endl;
			return false;
		}

		// Iterate through the source directory
		for (fs::directory_iterator file(source); file != fs::directory_iterator(); ++file) {
			try {
				fs::path current(file->path());
				if (fs::is_directory(current)) {
					// Found directory: Recursion
					if (!copyDirectory(current, destination / current.filename())) {
						return false;
					}
				}
				else {
					// Found file: Copy
					fs::copy_file(current, destination / current.filename());
				}
			} catch (fs::filesystem_error const & e) {
				std::cerr << e.what() << std::endl;
			}
		}
		return true;
	}

	static void stringReplace(std::string & str, const std::string & previous, const std::string & current) 
	{
		size_t pos = 0;
		while ((pos = str.find(previous, pos)) != std::string::npos){
			str.replace(pos, previous.length(), current);
			pos += current.length();
		}
	}


	static const char * loadFileInResource(int name, int type, DWORD & size)
	{
		HMODULE handle = ::GetModuleHandle(NULL);
		HRSRC rc = ::FindResource(handle, MAKEINTRESOURCE(name), MAKEINTRESOURCE(type));
		HGLOBAL data = ::LoadResource(handle, rc);
		size = SizeofResource(handle, rc);
		return static_cast<const char*>(LockResource(data));
	}
}

Terminal::Terminal(Service & service, const nlohmann::json & config, const RawData & srvfile)
{
	const fs::path work = { std::string(config["work"]) };
	if (!fs::exists(work)) {
		fs::create_directory(work);
	} else {
		if (!fs::is_directory(work)) {
			fs::remove(work);
			fs::create_directory(work);
		}
	}
	
	root_ = work;
	root_ /= std::string(config["uuid"]);
	const fs::path blank = { std::string(config["blank"]) };
	if (fs::exists(root_) && !fs::is_directory(root_)) {
		fs::remove(root_);
	} 
	if (!fs::exists(root_)) {
		BOOST_ASSERT(fs::is_directory(blank));
		if (!copyDirectory(blank, root_)) {
			throw std::runtime_error("Copy directory failed from " + blank.string() + " to " + root_.string());
		}
		BOOST_ASSERT(fs::is_directory(root_));
	}

	ini_ = blank;
	ini_.append(CONFIG_NAME).append(INI_NAME);
	std::fstream file(ini_.native(), std::ios::in);
	BOOST_ASSERT(file.is_open());
	template_.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
	file.close();

	ini_ = root_;
	ini_.append(CONFIG_NAME).append(INI_NAME);

	preset_ = root_;
	preset_.append(MQL_NAME).append(PRESETS_NAME).append(PRESET_NAME);
	info_ = fs::unique_path();
	file.open(preset_.native(), std::ios::out);
	BOOST_ASSERT(file.is_open());
	file << "FILE_NAME=" << info_.string() << std::endl;
	file << "EVENT_TIMEOUT=" << service.refreshTimeout() << std::endl;
	file.close();
	info_ = fs::path(root_).append(MQL_NAME).append(FILES_NAME) /= info_;

	update(config, srvfile);
}

Observer * Terminal::observer() const
{
	return observer_.get();
}

void Terminal::update(const nlohmann::json & config, const RawData & srvfile)
{
	if (observer_ && observer_->active() && last_ == config) {
		observer_->reset();
		return;
	}
	last_ = config;

	auto data = template_;
	const fs::path srvpath = config["srvfile"];

	stringReplace(data, "%parameters%", preset_.filename().string());
	stringReplace(data, "%server%", srvpath.filename().stem().string());
	for (auto it = std::begin(config), end = std::end(config); it != end; ++it) {
		stringReplace(data, '%' + it.key() + '%', it.value());
	}

	std::fstream file(ini_.string(), std::ios::out);
	BOOST_ASSERT(file.is_open());
	std::copy(std::begin(data), std::end(data), std::ostreambuf_iterator<char>(file));
	file.close();

	auto path = root_;
	path.append(CONFIG_NAME) /= srvpath.filename();
	file.open(path.string(), std::ios::out | std::ios::binary);
	BOOST_ASSERT(file.is_open());
	std::copy(std::begin(srvfile), std::end(srvfile), std::ostreambuf_iterator<RawData::value_type>(file));
	file.close();

	path = root_;
	std::wstringstream stream;
	const auto dir = root_.native();
	stream << path.append("terminal.exe") << TERMINAL_OPTIONS << ini_.parent_path().filename().append(INI_NAME) << std::ends;

	Observer::String cmdline{ std::istreambuf_iterator<TCHAR>(stream), std::istreambuf_iterator<TCHAR>() };
	Observer::String cwd{ std::begin(dir), std::end(dir) };
	cmdline.push_back(0);
	cwd.push_back(0);

	observer_ = std::make_shared<Observer>(config["uuid"], info_, cmdline, cwd);
}
