#include "stdafx.h"
#include "Service.hpp"
#include "Observer.hpp"

namespace
{
	static const std::vector<std::string> VARS = { "work", "blank" };
}

Service::Service(nlohmann::json & config) :
config_(config)
{
}

int Service::refreshTimeout() const
{
	return config_["refresh"];
}

Observer * Service::start(const std::string & header, const Terminal::RawData & data)
{
	try {
		auto json = nlohmann::json::parse(header);
		const std::string uuid = json["uuid"];
		const auto it = terminals_.find(uuid);

		for (const auto var : VARS) {
			json[var] = (config_.find(var) == config_.end())
				? boost::filesystem::current_path().string()
				: config_[var];
		}

		std::shared_ptr<Terminal> terminal;

		if (it == terminals_.end()) {
			terminal = std::make_shared<Terminal>(*this, json, data);
			terminals_[uuid] = terminal;
		} else {
			terminal = it->second;
			terminal->update(json, data);
		}
		
		return terminal->observer();
	
	} catch (const std::exception & exception) {
		std::cerr << exception.what() << std::endl;
	}

	return nullptr;
}


Observer * Service::restart(const std::string & uuid)
{
	const auto it = terminals_.find(uuid);
	if (it != terminals_.end()) {
		const auto observer = it->second->observer();
		if (observer && observer->active()) {
			observer->abort();
		}
		return observer;
	}
	return nullptr;
}

void Service::shutdown()
{
	for (const auto & pair : terminals_) {
		const auto observer = pair.second->observer();
		if (observer && observer->active()) {
			observer->abort();
		}
	}
}
