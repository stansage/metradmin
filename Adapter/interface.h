#pragma once

//! Declares the exported functions for the DLL application.

#ifdef ADAPTER_EXPORTS
#define ADAPTER_API __declspec(dllexport)
#else
#define ADAPTER_API __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C" {
#endif
	ADAPTER_API uint32_t __stdcall AttachAdapter(int32_t handle);
	ADAPTER_API void __stdcall DetachAdapter(uint32_t adapter);
	ADAPTER_API uint32_t __stdcall ReadAdapter(uint32_t adapter);
	ADAPTER_API void __stdcall WriteAdapter(uint32_t adapter, uint32_t packet);
	ADAPTER_API uint64_t NanoTime();

#ifdef __cplusplus
}
#endif

using String = std::basic_string<TCHAR>;
using FileStream = std::basic_fstream<TCHAR>;
template<typename T>
String toString(T && value)
{
#ifdef _UNICODE
	return std::to_wstring(value);
#else
	return std::to_string(value);
#endif
}

static const std::vector<std::string> PACKETS = {
	{ '0', '0', '0', '0', '0', '0', '0', '0', '0' },
	{ ',', ',', ',', 'c', 'b', 0, 0, 0x08, 0x40 },
	{ 'B', 'B', 'B', 'c', 'b', 0, 0, 0x08, 0x40 },
	{ '!', '!', '!', 'c', 'b', 0, 0, 0x08, 0x40 },
};

std::string flogPath();
FileStream & flog();
