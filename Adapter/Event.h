#pragma once


class Event
{
public:
	Event(const TCHAR * name);
	~Event();

	bool wait(unsigned long timeout);
	void notify();

private:
	void raiseError();

private:
	HANDLE m_handle;
};

