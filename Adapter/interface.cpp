#include "stdafx.h"
#include "interface.h"
#include "Adapter.h"

namespace
{
	std::unordered_map<uint32_t, std::shared_ptr<Adapter> > adapters;
	const uint64_t frequency = []() -> uint64_t
	{
		LARGE_INTEGER frequency;
		QueryPerformanceFrequency(&frequency);
		return frequency.QuadPart;
	}();
}

uint32_t __stdcall AttachAdapter(int32_t handle)
{
	using Limits = std::numeric_limits<uint32_t>;

	std::random_device random;
	std::mt19937 generator(random());
	std::uniform_int_distribution<uint32_t> distribution(Limits::min() + 1, Limits::max());
	auto result = Limits::min();

	auto attempts = 10;
	while (result == Limits::min() && attempts != 0) {
		auto id = distribution(generator);
		if (adapters.find(id) == adapters.end()) {
			try {
				adapters[id] = std::make_shared<Adapter>(handle);
				result = id;
			} catch (std::exception & exception) {
				--attempts;
				flog() << " [" << id << "] warning create " << exception.what() << handle << std::endl;
			}
		}
	}

	return result;
}

void __stdcall DetachAdapter(uint32_t adapter)
{
	auto it = adapters.find(adapter);
	if (it != adapters.end()) {
		try {
			it->second->detach();
		} catch (std::exception & exception) {
			flog() << " [" << adapter << "] warning detach " << exception.what() << std::endl;
		}
		adapters.erase(it);
	}
}

ADAPTER_API uint32_t __stdcall ReadAdapter(uint32_t adapter)
{
	uint32_t result = 0;
	const auto it = adapters.find(adapter);
	if (it != adapters.end()) {
		try {
			result = it->second->read();
		} catch (std::exception & exception) {
			flog() << " [" << adapter << "] warning read " << exception.what() << std::endl;
		}
	}

	return result;
}

ADAPTER_API void __stdcall WriteAdapter(uint32_t adapter, uint32_t packet)
{
	const auto it = adapters.find(adapter);
	if (it != adapters.end()) {
		try {
			it->second->write(packet);
		} catch (std::exception & exception) {
			flog() << " [" << adapter << "] warning write " << exception.what() << std::endl;
		}
	}
}

uint64_t NanoTime()
{
	LARGE_INTEGER counter;
	QueryPerformanceCounter(&counter);

	auto result = counter.QuadPart;
	auto ns = static_cast<double>(counter.QuadPart);

	result /= frequency;
	ns /= frequency;
	ns -= result;

	result *= std::nano::den;
	result += static_cast<uint64_t>(ns * std::nano::den);

	return result;
}

std::string flogPath()
{
	std::stringstream path;
	path << getenv("TEMP") << '\\' << GetCurrentProcessId() << "_Adapter.log";
	return path.str();
}

FileStream & flog()
{
	static FileStream result;

	if (result.is_open()) {
		result << NanoTime();
	}

	return result;
}

