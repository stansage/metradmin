#include "stdafx.h"
#include "ServerPipe.h"

ServerPipe::ServerPipe(int port, ipc::permissions & permissions) :
Pipe(permissions),
m_socket(m_ios, Udp::endpoint(Udp::v4(), port)),
m_thread(std::bind(&ServerPipe::run, this))
{
}

void ServerPipe::close()
{
	flog() << " stopping server" << std::endl;
	
	write(PACKETS[0]);
	m_socket.close();
	m_ios.stop();
	m_thread.join();
}

void ServerPipe::run()
{
	flog() << " starting server" << std::endl;

	startReceive();

	boost::system::error_code error;
	const auto size = m_ios.run(error);
	if (error) {
		flog() << size << " error " << error.message().c_str() << std::endl;
	}

	flog() << " server stopped" << std::endl;
}

void ServerPipe::startReceive()
{
	namespace asio = boost::asio;
	namespace phs = asio::placeholders;


	if (m_socket.is_open()) {
		m_socket.async_receive_from(asio::buffer(m_buffer), m_remote, [this](const ErrorCode & error, std::size_t size){
			if (error) {
				flog() << size << " warning receive " << error << " - " << error.message().c_str() << std::endl;
			} else if (size >= m_buffer.static_size) {
				flog() << " received " << size << " bytes" << std::endl;
				write(&m_buffer[0], size);
			}
			startReceive();
		});
	}
}
