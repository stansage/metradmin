#pragma once

//! Declares the class Adapter for the interface implementation.
#include "interface.h"

class Pipe;

class Adapter
{
public:
	explicit Adapter(int32_t handle);

	void detach();
	uint32_t read();
	void write(uint32_t packet);

private:
	using Mutex = std::mutex;
	using Lock = std::unique_lock<Mutex>;
	
	Mutex m_mutex;
	std::shared_ptr<Pipe> m_pipe;
	std::unordered_map<std::string, uint32_t> m_packets;
};