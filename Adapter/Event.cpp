#include "stdafx.h"
#include "Event.h"
#include "interface.h"

Event::Event(const TCHAR * name)
{
	m_handle = OpenEvent(EVENT_ALL_ACCESS, TRUE, name);
	if (!m_handle) {
		m_handle = CreateEvent(NULL, FALSE, FALSE, name);
		if (!m_handle) {
			raiseError();
		}
	}
}

Event::~Event()
{
	CloseHandle(m_handle);
}

bool Event::wait(unsigned long timeout)
{
	switch (WaitForSingleObject(m_handle, timeout)) {
	case WAIT_TIMEOUT:
		return false;

	case WAIT_OBJECT_0:
		return true;
	}

	raiseError();
	return false;
}

void Event::notify()
{
	SetEvent(m_handle);
}

void Event::raiseError()
{
	throw boost::system::system_error(GetLastError(), boost::system::get_system_category());
}