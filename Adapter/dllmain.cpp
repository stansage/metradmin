// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "interface.h"

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		flog().open(flogPath().c_str(), std::ios::out);
		flog() << " DLL_PROCESS_ATTACH " << hModule << std::endl;
		break;

	case DLL_THREAD_ATTACH:
		flog() << " DLL_THREAD_ATTACH " << hModule << std::endl;
		break;

	case DLL_THREAD_DETACH:
		flog() << " DLL_THREAD_DETACH " << hModule << std::endl;
		break;

	case DLL_PROCESS_DETACH:
		flog() << " DLL_PROCESS_DETACH " << hModule << std::endl;
		flog().close();
		break;
	}
	return TRUE;
}

