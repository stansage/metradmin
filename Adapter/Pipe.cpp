#include "stdafx.h"
#include "Pipe.h"

Pipe::Pipe(ipc::permissions & permissions) :
m_sync(_T("MeTrAdmin.Event")),
m_share(ipc::open_or_create, "MeTrAdmin.SharedMemory", ipc::read_write, MemorySize, permissions),
m_shm(m_share, ipc::read_write)
{
	m_data.reserve(static_cast<std::size_t>(m_share.get_size()));
}

const std::string & Pipe::read()
{
	if (!m_sync.wait(ReadTimeout)) {
		m_data.clear();
	} else {
		const auto data = static_cast<char *>(m_shm.get_address());
		m_data.assign(data + 1, data[0]);
	}

	return m_data;
}

void Pipe::write(const std::string & data)
{
	write(data.c_str(), data.size());
}

void Pipe::write(const char * data, std::size_t size)
{
	const auto limit = std::min(m_shm.get_size() - 1, size);
	if (size > limit) {
		flog() << " Packet too big " << size << " and will be truncated to " << limit << std::endl;
		size = limit;
	}

	auto shm = static_cast<char*>(m_shm.get_address());
	shm[0] = size;
	std::memcpy(shm + 1, data, size);

	m_sync.notify();
}
