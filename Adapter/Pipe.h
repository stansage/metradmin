#pragma once

#include "interface.h"
#include "Event.h"

namespace ipc = boost::interprocess;

class Pipe
{
public:
	static const auto PacketSize = 9;
	static const auto MemorySize = 256;
	static const auto ReadTimeout = 999;

	Pipe(ipc::permissions & permissions);

	virtual void close() = 0;

	const std::string & read();
	void write(const std::string & data);
	void write(const char * data, std::size_t size);

private:
	Event m_sync;
	ipc::windows_shared_memory m_share;
	ipc::mapped_region m_shm;
	std::string m_data;
};

