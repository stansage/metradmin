#include "stdafx.h"
#include "Adapter.h"
#include "ServerPipe.h"
#include "ClientPipe.h"
//#include "Window.h"

Adapter::Adapter(int32_t handle)
{
	ipc::permissions permissions;
	permissions.set_unrestricted();
	if (handle < 0) {
		flog() << " creating server pipe" << std::endl;
		m_pipe = std::make_shared<ServerPipe>(-handle, permissions);
	} else {
		flog() << " creating client pipe" << std::endl;
		m_pipe = std::make_shared<ClientPipe>(permissions);
	}
	
	for (auto i = 0u; i < PACKETS.size(); ++i) {
		m_packets[PACKETS[i].substr(0, 3)] = i;
	}
}

void Adapter::detach()
{
	flog() << " detaching" << std::endl;
	m_pipe->close();
}

uint32_t Adapter::read()
{
	uint32_t result = 0;

	const auto data = m_pipe->read();
	if (data.empty()) {
		result = -1;
	} else {
		if (data.size() > Pipe::PacketSize) {
			flog() << " warning big packet " << data.size() << std::endl;
		}
		const auto it = m_packets.find(data.substr(0, 3));
		if (it != m_packets.end()) {
			result = it->second;
		} else {
			auto & f = flog() << " warning uknown packet ";
			for (const auto c : data) {
				f << std::hex << static_cast<int>(c);
			}
			f << std::endl;
		}
	}

	if (result != -1) {
		flog() << " read " << result << std::endl;
	}

	return result;
}

void Adapter::write(uint32_t packet)
{
	if (packet < PACKETS.size()) {
		flog() << " write " << packet << std::endl;
		m_pipe->write(PACKETS[packet]);
	}
}
