#pragma once
#include "Pipe.h"

class ServerPipe :
	public Pipe
{
	using ErrorCode = boost::system::error_code;

public:
	ServerPipe(int port, ipc::permissions & permissions);

	void close() override;

private:
	void run();
	void startReceive();
	
private:
	using Udp = boost::asio::ip::udp;
	
	boost::asio::io_service m_ios;
	Udp::socket m_socket;
	Udp::endpoint m_remote;
	boost::array<char, PacketSize> m_buffer;
	std::thread m_thread;
};

