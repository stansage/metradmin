//+------------------------------------------------------------------+
//|                                                   _olyakish_.mq4 |
//|                                                         olyakish |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "3-10  ������� ����� ��� ����� �� ������� ������� �� ������� ������� ������ ��������"
//#property strict

#property link      ""
/*
   1-02  ������� ��������� 
   1-05  ������ ������� ��� ����� � ������
         ������ ������� ��� ����� � ������
   1-06  ����������� � ������������ ��  
   1-08  ���������� ����������� � ������� ������ �� close[1] ����� �� ������� �� ���  + ������ �� ���� � ���� � ����������� � ����������� �� �����������     
   1-09  ������ ���� ��� ������� � �������
   2-00  ��������� ����
   2-01  50% ��������
   2-03
   3-01  ��������� ��� �������� ��� 6�� ���������� ��������
   3-03  ��������� - ��������� ������
   3-04  ��������� ���� ������ ��������� (� ����������)
   3-05  Event � ���
   3-06  ����� ����������� ������ ��� ��������� ��������
   3-07  ���� �������� ��������� - �� ������ ������ 
   3-08  ������� ���������� �������������
   3-09  ����������� �� ���� ��������� -���� ����� ������ �� �� �� �����������
   3-10  ������� ����� ��� ����� �� ������� ������� �� ������� ������� ������ �������� 
   
   
*/

//---- input parameters

extern double LotNormal=0.1;     // ��� ��� ���������� ��������
extern double LotAgress=0.1;     // ��� ��� ����������� ��������
extern double LotTest=0.1;     // ��� ��� �������� ��������

extern int sleeppageNormal=15;         // ���� �� ���������� 
extern int sleeppageAgress=10;         // ���� �� ���������� Agress
extern int sleeppageTest=10;         // ���� �� ���������� ����

extern int  tp_Ot_pr_open_posNormal=50;                // ������ �� ���� �������� ������ 
extern int  tp_Ot_pr_open_posAgress=40;                // ������ �� ���� �������� ������ 
extern int  tp_Ot_pr_open_posTest=30;                 // ������ �� ���� �������� ������ 
extern int  SL_Ot_pr_open_pos=30;                // ���� ���� �� ���� �������� ������ 
                                                 //extern string rem002="��������� (-1 - ��������)";
extern int Kogda_stavit_BU=8;               // ����� ������� ������� ��
extern int point_BU=1;                       // ������� ������� ���������
extern bool ReversInputSignal=false;         // ������ ��� �������� ��������
extern double NormalSpread=1.8;              // ���������� �����


int  msSleep=2;            // ����� � �� ��� ����� ����� ����������� 
int  magic=0;

int maxPipsThenNotTradeNormal;           // ������������ �������� ������� ����� ����� ��������� ���� ���������� 
int maxPipsThenNotTradeAgress;           // ������������ �������� ������� ����� ����� ��������� ���� ���������� 
int maxPipsThenNotTradeTest;             // ������������ �������� ������� ����� ����� ��������� ���� ���������� 


                                         //extern string rem001="������������� ��������� ������� (������,��)";
//extern bool followOpenPos=true;    // ��������� ������������� �������� �������  (��� ����� ��������� ����� � ������� ���������������
//extern int  tp_Ot_pr_open_bar=-1;            // ������ �� �� �������� �� �� ������ ����� -1 ��������� 




#include <stdlib.mqh>
#define ERROR_FILE_NOT_FOUND        2 
#define modeOpen								0 // ���� ��������
#define modeCreate							1 // ���� ��������

#import "Dll_prjVS.dll"
int MemOpen(int typeFile,int size,int mode,int &err[]); // ���������/������� ���� � ������, �������� �����
void MemClose(int hmem); // ��������� ���� � ������
int MemWrite(int hmem,int &v[],int pos,int sz,int &err[]); // ������ int(4) ������� v � ������ � ��������� ������� pos, �������� sz
ulong MemRead(int hmem,int &v[], int pos, int sz, int &err[]); // ������ ������� v � ��������� ������� pos �������� sz
int MemReadLocal(int hmem,int &v[],int pos,int sz,int &err[]); // ������ ������� v � ��������� ������� pos �������� sz
#import

int time;
bool up,dn;
// ������ �����
int hmemLocal=-1,hmemGlobal=-1,err[1]; // ������� ������
string fileLocal,fileGlobal; // ��� ����� � ������
int globalTicket=0;        // ����� ���������� ��� 
double priceOpenNewsBar=-1.0; // ���� �������� ���������� ����
int last_mod_sec;
int lastGetTickCount=0,result=0;
int CountImpulse=0;
int timestartsend;
int mult=1;
bool BUModifyOk;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int init()
  {
   MathSrand((int)TimeLocal());
   magic=MathRand()+1;
   maxPipsThenNotTradeNormal=sleeppageNormal;
   maxPipsThenNotTradeAgress=sleeppageAgress;
   maxPipsThenNotTradeTest=sleeppageTest;

   globalTicket=0;
   BUModifyOk=false;
   mult=1;
   if(Digits==3 || Digits==5){mult=10;}
   MathSrand(1);
   fileGlobal="Global\\All"; // ��� ����� � ������
   fileLocal="Local\\123"; // ��� ����� � ������
   Print(" magic=",magic);
   hmemGlobal=memOpen(fileGlobal,100);
   hmemLocal=memOpen(fileLocal,200);
   return(0);
  }
//********************************************************************************************
int  memOpen(string name,int cmdID)
  {
   int handle=-50;
   Print("start open ",name);
   handle=MemOpen(cmdID,-1,modeOpen,err); // ��������� ��������� ����
   if(handle>0) // ���� �������
     {
      Print("open OK ",name," h=",handle);
      Comment("Server is running\nMaxLot=",calcMaxLot());
     }
   else
   if(err[0]==ERROR_FILE_NOT_FOUND) // ���� �� �������,
     {
      Print("-err("+err[0]+") memfile not found.");
      Comment("Server IS STOP");
     }
   else
     {
      Print("-unknow err("+err[0]+")  h="+handle);
      Comment("Server IS STOP");
      return(-50);
     }
   return(handle);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool ���������������(int type,double inLot,int prolet,int profit,int sleepp)
  {
   double pr_open,_TP,_SL;
//int ticket=0;
   BUModifyOk=false;
   int startOpenMS=0,openMS=0,startmodyMS=0,modyMS=0;
   if(!IsTradeContextBusy())
     {
      RefreshRates();
      if(type==0){pr_open=NormalizeDouble(Ask,Digits);}
      if(type==1){pr_open=NormalizeDouble(Bid,Digits);}
      if((type==0 && (pr_open-(Close[1]+NormalSpread*Point*mult))>prolet*Point*mult) || //  ������� ��� ��� �� ������� ����� ��� �������
         (type==1 && (Close[1]-pr_open)>prolet*Point*mult))   // ������� ��� ��� �� ���� ���� ��� �������                 
         // ��� ������ �������
        {
         Print(pr_open," ",Close[1]," ",pr_open-Close[1]," "," ���� ��������� ",MathAbs(Close[1]-pr_open)/Point/mult,"  ���������=",prolet," �������");
         return(false);
        }
      startOpenMS=GetTickCount();
      timestartsend=GetTickCount();
      globalTicket=OrderSend(Symbol(),type,inLot,pr_open,sleepp*mult,0.0,0.0,"",magic,0,DarkOrange);
      openMS=GetTickCount();
      if(globalTicket<0){Print("������ �������� ������",GetLastError());}
      if(globalTicket>0)
        {
         startmodyMS=GetTickCount();
         OrderSelect(globalTicket,SELECT_BY_TICKET,MODE_TRADES);
         if(type==0){_TP=OrderOpenPrice()+profit*Point*mult;_SL=OrderOpenPrice()-SL_Ot_pr_open_pos*Point*mult;}
         if(type==1){_TP=OrderOpenPrice()-profit*Point*mult;_SL=OrderOpenPrice()+SL_Ot_pr_open_pos*Point*mult;}
         OrderModify(globalTicket,OrderOpenPrice(),_SL,_TP,0,Aqua);
         modyMS=GetTickCount();
        }
      Print("cmd=",type," ������ �� ����=",DoubleToStr(pr_open,Digits)," � ��������� �� ����=",DoubleToStr(OrderOpenPrice(),Digits)," _SL=",DoubleToStr(_SL,Digits)," _TP=",DoubleToStr(_TP,Digits)," lot=",inLot);
      Print("����� �� �������� =",openMS-startOpenMS," ����� �� �����������=",modyMS-startmodyMS);
      priceOpenNewsBar=Open[0];
     }
   return(0);
  }
//********************************************************************************************

bool ���������������(int type,int percent)
  {
   double lotClose=0.0;
   if(OrdersTotal()!=0) // � ��� ���� �������� ������
     {
      for(int i=OrdersTotal()-1;i>=0;i--)
        {
         RefreshRates();
         OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
         if(OrderType()==type && OrderMagicNumber()==magic && OrderSymbol()==Symbol() && type==OP_BUY)
           {
            if(percent==100){OrderClose(OrderTicket(),OrderLots(),Bid,5,Lime);}
            if(percent==50) {OrderClose(OrderTicket(),(NormalizeLot((OrderLots()/2),True,NULL)),Bid,5,Lime);}
           }
         if(OrderType()==type && OrderMagicNumber()==magic && OrderSymbol()==Symbol() && type==OP_SELL)
           {
            if(percent==100){OrderClose(OrderTicket(),OrderLots(),Ask,5,Lime);}
            if(percent==50) {OrderClose(OrderTicket(),(NormalizeLot((OrderLots()/2),True,NULL)),Ask,5,Lime);}
           }
        }
      // ����� ������ ��� ��������� �������� ��� ��� ���������� ��������
      globalTicket=0;
      for(i=OrdersTotal()-1;i>=0;i--)
        {
         OrderSelect(i,SELECT_BY_POS,MODE_TRADES);
         if(OrderMagicNumber()==magic && OrderSymbol()==Symbol() && (OrderType()==OP_BUY || OrderType()==OP_SELL))
           {globalTicket=OrderTicket();}
        }

     }
   return(0);
  }
//********************************************************************************************
bool SupportPos()
  {
///----���������
   if(OrderSelect(globalTicket,SELECT_BY_TICKET,MODE_TRADES) && Kogda_stavit_BU>0 && !BUModifyOk)
     {
      if(OrderType()==OP_BUY && OrderStopLoss()<OrderOpenPrice())
        {
         if((OrderOpenPrice()+Kogda_stavit_BU*Point*mult<MarketInfo(Symbol(),MODE_BID)) || // ���� ���� ������� + ������ �� �� ������ ��������  ����
            (OrderOpenPrice()+Kogda_stavit_BU*Point*mult<High[0])) // ���� ���������� ��� �� �������� ��� �����  
           {
            Print("��������� ������� � ���������");
            int startCount=GetTickCount();
            if(OrderModify(globalTicket,OrderOpenPrice(),OrderOpenPrice()+point_BU*mult*Point,OrderTakeProfit(),0,Yellow))
              {
               BUModifyOk=true;
               Print("�������������� ������� ��������� ",GetTickCount()-startCount,"ms");
              }
            Print("��������� ������� � ���������"+globalTicket+" "+DoubleToStr(OrderOpenPrice(),Digits)+" "+
                  DoubleToStr((OrderOpenPrice()+point_BU*mult*Point),Digits)+" "+DoubleToStr(OrderTakeProfit(),Digits));
           }
        }
      if(OrderType()==OP_SELL && OrderStopLoss()>OrderOpenPrice())
        {
         if((OrderOpenPrice()-Kogda_stavit_BU*Point*mult>MarketInfo(Symbol(),MODE_ASK)) || // ���� ���� ������� + ������ �� �� ������ ��������  ����
            (OrderOpenPrice()-Kogda_stavit_BU*Point*mult>Low[0]+(MarketInfo(Symbol(),MODE_ASK)-MarketInfo(Symbol(),MODE_BID)))) // ���� ���������� Ask �� �������� low ����� + spread 
           {
            Print("��������� ������� � ���������");
            startCount=GetTickCount();
            if(OrderModify(globalTicket,OrderOpenPrice(),OrderOpenPrice()-point_BU*mult*Point,OrderTakeProfit(),0,Yellow))
              {
               BUModifyOk=true;
               Print("�������������� ������� ��������� ",GetTickCount()-startCount,"ms");
              }
            Print("��������� ������� � ���������"+globalTicket+" "+DoubleToStr(OrderOpenPrice(),Digits)+" "+
                  DoubleToStr((OrderOpenPrice()+point_BU*mult*Point),Digits)+" "+DoubleToStr(OrderTakeProfit(),Digits));
           }
        }
     }
   return(true);
  }
//********************************************************************************************
//+----------------------------------------------------------------------------+
//|  �����    : ��� ����� �. aka KimIV,  http://www.kimiv.ru                   |
//+----------------------------------------------------------------------------+
//|  ������   : 16.05.2008                                                     |
//|  �������� : ���������� ��������������� �������� ���������� ����.           |
//+----------------------------------------------------------------------------+
//|  ���������:                                                                |
//|    lo - ������������� �������� ����.                                       |
//|    ro - ������ ����������          (   False    - � �������,               |
//|                                        True     - � ������� �������)       |
//|    sy - ������������ �����������   ("" ��� NULL - ������� ������)          |
//+----------------------------------------------------------------------------+
double NormalizeLot(double lo,bool ro=True,string sy="")
  {
   double l,k;
   if(sy=="" || sy=="0") sy=Symbol();
   double ls=MarketInfo(sy, MODE_LOTSTEP);
   double ml=MarketInfo(sy, MODE_MINLOT);
   double mx=MarketInfo(sy, MODE_MAXLOT);

   if(ml==0) ml=0.1;
   if(mx==0) mx=100;

   if(ls>0) k=1/ls; else k=1/ml;
   if(ro) l=MathCeil(lo*k)/k; else l=MathFloor(lo*k)/k;

   if(l<ml) l=ml;
   if(l>mx) l=mx;

   return(l);
  }
//********************************************************************************************
int start()
  {
   bool newtrade=true;

   if(hmemLocal<=0 || hmemGlobal<=0)
     {
      hmemGlobal=memOpen(fileGlobal,100);
      hmemLocal=memOpen(fileLocal,200);
     }

   int hreadLocal[2],hreadGlobal[2];

   bool lastSignalIsTrade=false;
   while(true && hmemLocal>0 && hmemGlobal>0 && IsExpertEnabled())
     {

      if(globalTicket>0)
        {int r=MemReadLocal(hmemLocal,hreadLocal,0,8,err);}
      if(globalTicket<=0) // ��� 5 ��� ����� 
        {ulong rG=MemRead(hmemGlobal,hreadGlobal,0,8,err);}
      if(globalTicket>0 && !BUModifyOk) {SupportPos();}

      if(hreadGlobal[0]==100 && globalTicket<=0){newtrade=true;Sleep(msSleep);lastSignalIsTrade=false;continue;}
      bool buyNormal=false,buyAgress=false,sellNormal=false,sellAgress=false,buyTest=false,sellTest=false;
      //----

      //----
      if(hreadLocal[0]==500 || hreadGlobal[0]==500){hmemLocal=-50;hmemGlobal=-50;Comment("Server NOT started");break;}
      if(!ReversInputSignal) // ����������
        {
         if((hreadGlobal[0]==111 || hreadLocal[0]==111) && newtrade){buyNormal=true;newtrade=false;}
         if((hreadGlobal[0]==122 || hreadLocal[0]==111) && newtrade){buyAgress=true;newtrade=false;}
         if((hreadGlobal[0]==133 || hreadLocal[0]==111) && newtrade){sellNormal=true;newtrade=false;}
         if((hreadGlobal[0]==144 || hreadLocal[0]==111) && newtrade){sellAgress=true;newtrade=false;}
         if((hreadGlobal[0]==155 || hreadLocal[0]==111) && newtrade){buyTest=true;newtrade=false;}
         if((hreadGlobal[0]==166 || hreadLocal[0]==111) && newtrade){sellTest=true;newtrade=false;}
        }
      if(ReversInputSignal) // �������� 
        {
         if((hreadGlobal[0]==111 || hreadLocal[0]==111) && newtrade){sellNormal=true;newtrade=false;}
         if((hreadGlobal[0]==122 || hreadLocal[0]==122) && newtrade){sellAgress=true;newtrade=false;}
         if((hreadGlobal[0]==133 || hreadLocal[0]==133) && newtrade){buyNormal=true;newtrade=false;}
         if((hreadGlobal[0]==144 || hreadLocal[0]==144) && newtrade){buyAgress=true;newtrade=false;}
         if((hreadGlobal[0]==155 || hreadLocal[0]==155) && newtrade){sellTest=true;newtrade=false;}
         if((hreadGlobal[0]==166 || hreadLocal[0]==166) && newtrade){buyTest=true;newtrade=false;}
        }

      // ������� �� �������� �������
      if(hreadLocal[0]==888 && globalTicket>0)
        {
         //if(PositionSelect(_Symbol))
           {
            ���������������(0,100); // �������� 100%
            ���������������(1,100); // �������� 100%
            Print("PositionClose 100%");
            Sleep(3000);
           }
        }
      // ������� �� �������� ������� 50%
      if(hreadLocal[0]==777 && globalTicket>0)
        {
         //if(PositionSelect(_Symbol))
           {
            ���������������(0,50); // �������� 50%
            ���������������(1,50); // �������� 50%
            Print("PositionClose 50%");
            Sleep(3000);
           }
        }

      // ������������� ������� 

      if(lastSignalIsTrade){Sleep(msSleep);continue;}
      if(buyNormal)
        {
         //timestartsend=GetTickCount();
         ���������������(0,LotNormal,maxPipsThenNotTradeNormal,tp_Ot_pr_open_posNormal,sleeppageNormal);
         Print("��������� ������ �� ����������� ��� �� ���� ",Ask," ������=",hreadGlobal[1]," ",GetTickCount()-hreadGlobal[1]," ����� ����� �� �������� � ����������� ��������=",GetTickCount()-timestartsend);
         Print("�������� ��=",rG);
         lastSignalIsTrade=true;
         buyNormal=false;
        }

      if(buyAgress)
        {
         //timestartsend=GetTickCount();
         ���������������(0,LotAgress,maxPipsThenNotTradeAgress,tp_Ot_pr_open_posAgress,sleeppageAgress);
         Print("��������� ������ �� ������������ ��� �� ���� ",Ask,"  ������=",hreadGlobal[1]," ",GetTickCount()-hreadGlobal[1]," ����� ���������� ������ ��������=",GetTickCount()-timestartsend);
         Print("�������� ��=",rG);
         lastSignalIsTrade=true;
         buyAgress=false;
        }
      if(sellNormal)
        {
         //timestartsend=GetTickCount();
         ���������������(1,LotNormal,maxPipsThenNotTradeNormal,tp_Ot_pr_open_posNormal,sleeppageNormal);
         Print("��������� ������ �� ����������� ���� �� ���� ",Bid,"  ������=",hreadGlobal[1]," ",GetTickCount()-hreadGlobal[1]," ����� ���������� ������ ��������=",GetTickCount()-timestartsend);
         Print("�������� ��=",rG);
         lastSignalIsTrade=true;
         sellNormal=false;
        }
      if(sellAgress)
        {
         //timestartsend=GetTickCount();
         ���������������(1,LotAgress,maxPipsThenNotTradeAgress,tp_Ot_pr_open_posAgress,sleeppageAgress);
         Print("��������� ������ �� ������������ ���� �� ���� ",Bid,"  ������=",hreadGlobal[1]," ",GetTickCount()-hreadGlobal[1]," ����� ���������� ������ ��������=",GetTickCount()-timestartsend);
         Print("�������� ��=",rG);
         lastSignalIsTrade=true;
         sellAgress=false;
        }
      if(buyTest)
        {
         //timestartsend=GetTickCount();
         ���������������(0,LotTest,maxPipsThenNotTradeTest,tp_Ot_pr_open_posTest,sleeppageTest);
         Print("��������� ������ �� ��������� ��� �� ���� ",Ask,"  ������=",hreadGlobal[1]," ",GetTickCount()-hreadGlobal[1]," ����� ���������� ������ ��������=",GetTickCount()-timestartsend);
         Print("�������� ��=",rG);
         lastSignalIsTrade=true;
         buyTest=false;
        }
      if(sellTest)
        {
         //timestartsend=GetTickCount();
         ���������������(1,LotTest,maxPipsThenNotTradeTest,tp_Ot_pr_open_posTest,sleeppageTest);
         Print("��������� ������ �� ��������� ���� �� ���� ",Bid,"  ������=",hreadGlobal[1]," ",GetTickCount()-hreadGlobal[1]," ����� ���������� ������ ��������=",GetTickCount()-timestartsend);
         Print("�������� ��=",rG);
         lastSignalIsTrade=true;
         sellTest=false;
        }
      if(IsStopped() || !IsExpertEnabled())
        {
         Print("stopped");
         break;
        }
      Sleep(msSleep);
     }//while
   return(0);
  }
//+------------------------------------------------------------------+
double calcMaxLot()
  {
   double ret=-1,testLot=0.0;//MarketInfo(Symbol(),MODE_MINLOT);
   for(int i=0;i<10000;i++)
     {
      if(AccountFreeMarginCheck(Symbol(),OP_BUY,testLot+MarketInfo(Symbol(),MODE_MINLOT))<=0){break;}
      testLot+=MarketInfo(Symbol(),MODE_MINLOT);
     }
   return(testLot);
  }
//+------------------------------------------------------------------+
