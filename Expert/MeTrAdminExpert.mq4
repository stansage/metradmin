//+------------------------------------------------------------------+
//|                                              MeTrAdminExpert.mq4 |
//|                                                         stansage |
//|                                            https://bitbacket.com |
//+------------------------------------------------------------------+
#property copyright "stansage"
#property link      "https://bitbucket.org/stansage/metradmin"
#property version   "1.00"
#property strict

#include <WinUser32.mqh>

#import "MeTrAdminAdapter.dll"
int AttachAdapter(int handle);
void DetachAdapter(int adapter);
int ReadAdapter(int adapter);
long NanoTime();
#import

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int adapter = 0;

int OnInit()
{
//--- Createa the adapter and starts the event loop
   adapter = AttachAdapter(ChartGetInteger(0, CHART_WINDOW_HANDLE));
   bool ok = adapter != 0 && EventSetTimer(1);
//--- Returns the result of call EventSetTimer 
   return (ok ? INIT_SUCCEEDED : INIT_FAILED);
}

//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
   Print("Deinit");
//---
   DetachAdapter(adapter);
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
//---
   PrintFormat("%I64d OnTick", NanoTime());
   
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTimer()
{
//---
   Print("Connecting...");
   int data = ReadAdapter(adapter);
   while (data != 0 && !IsStopped()) {
      switch (data) {
      case 2:
         OrderSend(Symbol(), OP_BUY, 1, Bid, 0, 0, 0);
         break;
      case 3:
         OrderSend(Symbol(), OP_SELL, 1, Bid, 0, 0, 0);
         break;
         
      }
      data = ReadAdapter(adapter);
   }
   if (data != 0) {
      Print("Stopped");
   }
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
{
//---
   PrintFormat("CHART EVENT %d %I64d %f %s", id, lparam, dparam, sparam);
}
//+------------------------------------------------------------------+
