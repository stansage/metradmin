//+------------------------------------------------------------------+
//|                                                         Init.mq4 |
//|                                                         stansage |
//|                                            https://bitbacket.com |
//+------------------------------------------------------------------+
#property copyright "stansage"
#property link      "https://bitbucket.org/stansage/metradmin"
#property version   "1.00"
#property strict

input string FILE_NAME;
input int EVENT_TIMEOUT;

#import "kernel32.dll"
int GetCurrentProcessId();
#import

int OnInit()
{
   WriteInfo();
   EventSetTimer(EVENT_TIMEOUT);

   return ( INIT_SUCCEEDED );
}

void OnDeinit(const int reason)
{
   EventKillTimer();
}

void OnTimer()
{
   WriteInfo();
}

void WriteInfo()
{
   static string suffix = ".tmp";
   string name = FILE_NAME + suffix;
   int file = FileOpen(name, FILE_WRITE | FILE_CSV);
   if (file != INVALID_HANDLE) {
      FileWrite(file, GetCurrentProcessId());
      FileWrite(file, AccountBalance(), AccountCurrency());
      FileClose(file);
      FileMove(name,  0, FILE_NAME,  FILE_REWRITE);
   }
}