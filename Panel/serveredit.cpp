#include "stdafx.h"
#include "serveredit.hpp"
#include "ui_serveredit.h"
#include "mainwindow.hpp"
#include "database.hpp"

namespace
{
	static const QString IP_RANGE = QStringLiteral("(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])");
	static const QString IP_REGEX = QStringLiteral("^%1\\.%1\\.%1\\.%1$");
	static const auto KEYS = { Database::SERVER_NAME, Database::SERVER_IP, Database::SERVER_PORT };
}

ServerEdit::ServerEdit(QWidget * parent)
	: QDialog(parent),
	ui_(new Ui::ServerEdit)
{
	ui_->setupUi(this);

	ui_->buttonSave->setIcon(MainWindow::getIcon(MainWindow::Icon::Save));
	ui_->buttonCancel->setIcon(MainWindow::getIcon(MainWindow::Icon::Cancel));
	
	ui_->lineEdit_1->setObjectName(Database::SERVER_NAME);
	ui_->lineEdit_2->setObjectName(Database::SERVER_IP);
	ui_->lineEdit_3->setObjectName(Database::SERVER_PORT);

	ui_->lineEdit_2->setValidator(new QRegExpValidator(QRegExp(IP_REGEX.arg(IP_RANGE)), this));
	ui_->lineEdit_3->setValidator(new QIntValidator(1, 65535, this));
}

ServerEdit::~ServerEdit()
{
	delete ui_;
}

void ServerEdit::load(const QJsonObject & json)
{
	for (const auto & key : KEYS) {
		const auto value = json[key];
		auto edit = findChild<QLineEdit*>(key);
		Q_ASSERT(edit != nullptr);
		
		if (value.isString()) {
			edit->setText(value.toString());
		} else {
			Q_ASSERT(value.isDouble() == true);
			edit->setText(QString::number(value.toInt()));
		}
	}
}

void ServerEdit::save(QJsonObject & json)
{
	for (const auto & key : KEYS) {
		auto edit = findChild<QLineEdit*>(key);
		Q_ASSERT(edit != nullptr);

		if (key != Database::SERVER_PORT) {
			json[key] = edit->text();
		} else {
			json[key] = edit->text().toUShort();
		}
	}
}

void ServerEdit::changeEvent(QEvent * e)
{
	if (e->type() == QEvent::LanguageChange) {
		ui_->retranslateUi(this);
	} else {
		QDialog::changeEvent(e);
	}
}

void ServerEdit::accept()
{
	for (const auto & key : KEYS) {
		auto edit = findChild<QLineEdit*>(key);
		Q_ASSERT(edit != nullptr);
		if (edit->text().isEmpty()) {
			edit->setFocus();
			return;
		}
	}
	QDialog::accept();
}