#pragma once

class TextEdit : public QLineEdit
{
	Q_OBJECT

public:
	TextEdit(QWidget * parent = nullptr);

protected:
	void focusInEvent(QFocusEvent * event) override;

signals:
	void focused();
};

