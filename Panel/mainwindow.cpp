#include "stdafx.h"
#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "config.hpp"
#include "database.hpp"
#include "transport.hpp"

MainWindow::MainWindow(QWidget * parent) :
QMainWindow(parent),
ui_(new Ui::MainWindow)
{
	ui_->setupUi(this);

	db_ = new Database(Config::database(), this);
	transport_ = new Transport(db_);

	connect(transport_, &Transport::connected, ui_->tab_3, &ServersWidget::onConnected);
	connect(transport_, &Transport::disconnected, ui_->tab_3, &ServersWidget::onDisconnected);
	connect(transport_, &Transport::raised, ui_->tab_2, &TerminalsWidget::raise);
	connect(transport_, &Transport::fell, ui_->tab_2, &TerminalsWidget::fall);

	connect(ui_->tab_2, &TerminalsWidget::restarted, transport_, &Transport::restart);

	connect(ui_->tab_3, &ServersWidget::removed, ui_->tab_2, &TerminalsWidget::onServerRemoved);
	connect(ui_->tab_3, &ServersWidget::changed, ui_->tab_2, &TerminalsWidget::onServerChanged);

	QTimer::singleShot(0, std::bind(&Transport::update, transport_));
}

MainWindow::~MainWindow()
{
    delete ui_;
}

Database * MainWindow::database() const
{
	return db_;
}

QIcon MainWindow::getIcon(Icon icon)
{
	QString name;

	switch (icon) {
	case Icon::NewDir:
		name = QStringLiteral("newdirectory");
		break;
	case Icon::NetDrive:
		name = QStringLiteral("networkdrive");
		break;
	case Icon::Save:
		name = QStringLiteral("standardbutton-save");
		break;
	case Icon::Apply :
		name = QStringLiteral("standardbutton-apply");
		break;
	case Icon::Reset:
		name = QStringLiteral("standardbutton-clear");
		break;
	case Icon::Cancel:
		name = QStringLiteral("standardbutton-cancel");
		break;
	case Icon::Refresh:
		name = QStringLiteral("refresh");
		break;
	case Icon::Edit:
		name = QStringLiteral("viewdetailed");
		break;
	case Icon::Close:
		name = QStringLiteral("standardbutton-close");
		break;
		
	case Icon::StatusGray:
		return QStringLiteral(":/icons/status-gray");
	case Icon::StatusGreen:
		return QStringLiteral(":/icons/status-green");
	case Icon::StatusRed:
		return QStringLiteral(":/icons/status-red");

	default:
		Q_ASSERT(int(icon) < int(Icon::Refresh) + 1);
	}
	
	return QStringLiteral(":/qt-project.org/styles/commonstyle/images/") + name + QStringLiteral("-32.png");
}

MainWindow * MainWindow::getInstance(QWidget * widget)
{
	Q_ASSERT(widget != nullptr);

	auto p = widget->parentWidget();

	Q_ASSERT(p != nullptr);
	for (widget = p->parentWidget(); widget != nullptr; widget = p->parentWidget()) {
		p = widget;
	}

	const auto result = qobject_cast<MainWindow*>(p);

	Q_ASSERT(p != nullptr);
	Q_ASSERT(result != nullptr);

	return result;
}

void MainWindow::changeEvent(QEvent * e)
{
	if (e->type() == QEvent::LanguageChange) {
		ui_->retranslateUi(this);
	} else {
		QMainWindow::changeEvent(e);
	}
}