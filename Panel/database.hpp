#pragma once

class Database : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QJsonArray servers READ servers WRITE setServers)
	Q_PROPERTY(QJsonArray terminals READ terminals WRITE setTerminals)

public:
	static const QString SERVER_NAME;
	static const QString SERVER_IP;
	static const QString SERVER_PORT;
	static const QString TERMINAL_UUID;
	static const QString TERMINAL_NAME;
	static const QString TERMINAL_SERVER;
	static const QString TERMINAL_TYPE;
	static const QString TERMINAL_LOGIN;
	static const QString TERMINAL_PASSWORD;
	static const QString TERMINAL_SYMBOL;
	static const QString TERMINAL_SRVFILE;
	static const QString TERMINAL_BALANCE;
	static const QString TERMINAL_CURRENCY;

	explicit Database(const QString & path, QObject * parent = nullptr);

	QJsonArray servers() const;
	void setServers(const QJsonArray & value);
	
	QJsonArray terminals() const;
	void setTerminals(const QJsonArray & value);

	void commit();

	int findServer(const QString & name, int skip = -1) const;
	int findTerminal(const QString & uuid, int skip = -1) const;

signals:
	void updated();

private:
	QFile * file_;
	QJsonObject root_;
};

