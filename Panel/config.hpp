#pragma once
class Config
{
public:
	static Config & instance();

	bool open(const QString & path);
	void save(const std::tuple<QString, int, int, QString> & values);
	
	static QString database();
	static int timeout();
	static int language();
	static QString style();

private:
	Config();
	~Config();

private:
	struct Impl;
	Impl * impl_;
};

