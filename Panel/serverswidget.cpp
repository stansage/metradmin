#include "stdafx.h"
#include "serverswidget.hpp"
#include "ui_serverswidget.h"
#include "mainwindow.hpp"
#include "database.hpp"
#include "serveredit.hpp"

namespace
{
	static const QString MESSAGE_TITLE = QStringLiteral("style");
}

ServersWidget::ServersWidget(QWidget * parent) :
    QWidget(parent),
    ui_(new Ui::ServersWidget)
{
    ui_->setupUi(this);

	ui_->buttonAdd->setIcon(MainWindow::getIcon(MainWindow::Icon::NetDrive));

	Q_ASSERT(ui_->table->columnCount() == 3);
	Q_ASSERT(ui_->table->horizontalHeader()->count() == 3);
	ui_->table->setHorizontalHeaderItem(2, new QTableWidgetItem());

	QTimer::singleShot(0, [this]() {
		const auto database = MainWindow::getInstance(this)->database();
		for (const auto entry : database->servers()) {
			Q_ASSERT(entry.isObject());

			const auto server = entry.toObject();
			const auto name = server[Database::SERVER_NAME].toString();

			addServer(name);
		}
	});
}

ServersWidget::~ServersWidget()
{
    delete ui_;
}

void ServersWidget::changeEvent(QEvent * e)
{
	if (e->type() == QEvent::LanguageChange) {
		ui_->retranslateUi(this);
	} else {
		QWidget::changeEvent(e);
	}
}

void ServersWidget::addServer(const QString & name)
{
	const auto row = ui_->table->rowCount();
	const auto icon = MainWindow::getIcon(MainWindow::Icon::StatusRed);
	const auto item = new QTableWidgetItem(name);

	ui_->table->setRowCount(row + 1);
	ui_->table->setItem(row, 1, item);
	ui_->table->setCellIcon(row, 0, icon);
	ui_->table->setCellButtons(row, 2, TableWidget::ButtonEdit | TableWidget::ButtonRemove);
}

void ServersWidget::onConnected(QString server)
{
	for (auto row = ui_->table->rowCount() - 1; row >= 0; --row) {
		if (ui_->table->item(row, 1)->text() == server) {
			const auto icon = MainWindow::getIcon(MainWindow::Icon::StatusGreen);
			ui_->table->setCellIcon(row, 0, icon);
		}
	}
}

void ServersWidget::onDisconnected(QString server)
{
	for (auto row = ui_->table->rowCount() - 1; row >= 0; --row) {
		const auto icon = MainWindow::getIcon(MainWindow::Icon::StatusRed);
		if (ui_->table->item(row, 1)->text() == server) {
			ui_->table->setCellIcon(row, 0, icon);
		}
	}
}

void ServersWidget::on_actionExists_triggered()
{
	const auto action = qobject_cast<QAction*>(sender());
	const auto name = action->data().toString();
	QMessageBox::warning(this, action->toolTip(), action->text().arg(name));
}

void ServersWidget::on_actionConfirm_triggered()
{
	const auto action = qobject_cast<QAction*>(sender());
	const auto row = action->data().toInt();
	const auto name = ui_->table->item(row, 1)->text();

	if (QMessageBox::Yes == QMessageBox::question(this, action->toolTip(), action->text().arg(name))) {
		const auto database = MainWindow::getInstance(this)->database();
		const auto index = database->findServer(name);
		auto servers = database->servers();

		Q_ASSERT(index != -1);
		
		servers.removeAt(index);
		database->setServers(servers);
		database->commit();

		ui_->table->removeRow(row);
		emit removed(name);
	}
}

void ServersWidget::on_buttonAdd_clicked()
{
	ServerEdit dialog(this);
	if (dialog.exec() != QDialog::Accepted) {
		return;
	};

	QJsonObject server;
	dialog.save(server);
		
	const auto database = MainWindow::getInstance(this)->database();
	const auto name = server[Database::SERVER_NAME].toString();
		
	if (database->findServer(name) != -1) {
		ui_->actionExists->setData(name);
		ui_->actionExists->trigger();
	} else {
		auto servers = database->servers();

		servers.append(server);
		database->setServers(servers);
		database->commit();

		addServer(name);
	}
}

void ServersWidget::on_table_edit(int row)
{
	const auto item = ui_->table->item(row, 1);
	const auto database = MainWindow::getInstance(this)->database();
	const auto index = database->findServer(item->text());

	Q_ASSERT(index != -1);

	ServerEdit dialog(this);
	auto servers = database->servers();
	
	dialog.load(servers[index].toObject());
	if (dialog.exec() != QDialog::Accepted) {
		return;
	}

	QJsonObject server;
	dialog.save(server);

	const auto name = server[Database::SERVER_NAME].toString();

	if (database->findServer(name, index) != -1) {
		ui_->actionExists->setData(name);
		ui_->actionExists->trigger();
	} else {
		servers.replace(index, server);
		database->setServers(servers);
		database->commit();

		if (name != item->text()) {
			emit changed(item->text(), name);
		}
		item->setText(name);
	}
}

void ServersWidget::on_table_remove(int row)
{
	ui_->actionConfirm->setData(row);
	ui_->actionConfirm->trigger();
}