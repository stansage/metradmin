#include "stdafx.h"
#include "database.hpp"

namespace
{
	static const QString KEY_SERVERS = QStringLiteral("servers");
	static const QString KEY_TERMINALS = QStringLiteral("terminals");
}

const QString Database::SERVER_NAME = QStringLiteral("name");
const QString Database::SERVER_IP = QStringLiteral("ip");
const QString Database::SERVER_PORT = QStringLiteral("port");
const QString Database::TERMINAL_UUID = QStringLiteral("uuid");
const QString Database::TERMINAL_NAME = QStringLiteral("name");
const QString Database::TERMINAL_SERVER = QStringLiteral("server");
const QString Database::TERMINAL_TYPE = QStringLiteral("type");
const QString Database::TERMINAL_LOGIN = QStringLiteral("login");
const QString Database::TERMINAL_PASSWORD = QStringLiteral("password");
const QString Database::TERMINAL_SYMBOL = QStringLiteral("symbol");
const QString Database::TERMINAL_SRVFILE = QStringLiteral("srvfile");
const QString Database::TERMINAL_BALANCE = QStringLiteral("balance");
const QString Database::TERMINAL_CURRENCY = QStringLiteral("currency");

Database::Database(const QString & path, QObject * parent) :
QObject(parent),
file_(new QFile(path))
{
	if (!file_->open(QFile::ReadWrite)) {
		qFatal("Failed to open database %s (%s)", path, file_->errorString());
	} else {
		QJsonParseError error;
		const auto json = QJsonDocument::fromJson(file_->readAll(), &error);
		if (error.error == QJsonParseError::NoError && json.isObject()) {
			root_ = json.object();
		}
	}
}

QJsonArray Database::servers() const
{
	return root_[KEY_SERVERS].toArray();
}

void Database::setServers(const QJsonArray & value)
{
	root_[KEY_SERVERS] = value;
}

QJsonArray Database::terminals() const
{
	return root_[KEY_TERMINALS].toArray();
}

void Database::setTerminals(const QJsonArray & value)
{
	root_[KEY_TERMINALS] = value;
}

void Database::commit()
{
	if (file_->resize(0) && file_->reset()) {
		QJsonDocument json(root_);
		file_->write(json.toJson());
		file_->flush();
		emit updated();
	} else {
		qCritical() << "Failed to commit database" << file_->fileName() << file_->errorString();
	}
}

int Database::findServer(const QString & name, int skip) const
{
	auto result = 0;
	for (const auto entry : servers()) {
		Q_ASSERT(entry.isObject());

		const auto server = entry.toObject();

		if (server[Database::SERVER_NAME].toString() == name) {
			if (skip == -1 || skip != result) {
				return result;
			}
		}
		++result;
	}

	return -1;
}

int Database::findTerminal(const QString & uuid, int skip) const
{
	auto result = 0;
	for (const auto entry : terminals()) {
		Q_ASSERT(entry.isObject());

		const auto terminal = entry.toObject();

		if (terminal[Database::TERMINAL_UUID].toString() == uuid) {
			if (skip == -1 || skip != result) {
				return result;
			}
		}
		++result;
	}

	return -1;
}
