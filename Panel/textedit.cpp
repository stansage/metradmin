#include "stdafx.h"
#include "textedit.hpp"

TextEdit::TextEdit(QWidget * parent)
	: QLineEdit(parent)
{
}

void TextEdit::focusInEvent(QFocusEvent * event)
{
	Q_ASSERT(event->gotFocus() == true);

	QLineEdit::focusInEvent(event);	
	emit focused();
}
