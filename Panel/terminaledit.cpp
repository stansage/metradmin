#include "stdafx.h"
#include "terminaledit.hpp"
#include "ui_terminaledit.h"
#include "mainwindow.hpp"
#include "database.hpp"
#include "config.hpp"

namespace
{
	static const auto EDIT_KEYS = { 
		Database::TERMINAL_NAME,
		Database::TERMINAL_LOGIN,
		Database::TERMINAL_PASSWORD,
		Database::TERMINAL_SYMBOL,
		Database::TERMINAL_SRVFILE 
	};
	static const auto LIST_KEYS = {
		Database::TERMINAL_TYPE,
		Database::TERMINAL_BALANCE,
	};
	static const QString SRV_FILTER = QStringLiteral("Server files (*.srv)");
}

TerminalEdit::TerminalEdit(QWidget * parent)
	: QDialog(parent),
	ui_(new Ui::TerminalEdit)
{
	ui_->setupUi(this);

	ui_->buttonSave->setIcon(MainWindow::getIcon(MainWindow::Icon::Save));
	ui_->buttonCancel->setIcon(MainWindow::getIcon(MainWindow::Icon::Cancel));

	ui_->lineEdit_1->setObjectName(Database::TERMINAL_NAME);
	ui_->lineEdit_2->setObjectName(Database::TERMINAL_LOGIN);
	ui_->lineEdit_3->setObjectName(Database::TERMINAL_PASSWORD);
	ui_->lineEdit_4->setObjectName(Database::TERMINAL_SYMBOL);
	ui_->lineEdit_5->setObjectName(Database::TERMINAL_SRVFILE);

	ui_->server->setObjectName(Database::TERMINAL_SERVER);
	ui_->type->setObjectName(Database::TERMINAL_TYPE);
	ui_->balance->setObjectName(Database::TERMINAL_BALANCE);

	if (parent != nullptr) {
		const auto database = MainWindow::getInstance(this)->database();
		for (const auto entry : database->servers()) {
			Q_ASSERT(entry.isObject());

			const auto server = entry.toObject();
			const auto name = server[Database::SERVER_NAME].toString();

			ui_->server->addItem(name);
		}
	}
}

TerminalEdit::~TerminalEdit()
{
	delete ui_;
}


void TerminalEdit::load(const QJsonObject & json)
{
	for (const auto & key : EDIT_KEYS) {
		const auto value = json[key];
		auto edit = findChild<QLineEdit*>(key);
		Q_ASSERT(edit != nullptr);

		edit->setText(value.toString());
	}

	for (const auto & key : LIST_KEYS) {
		const auto list = findChild<QComboBox*>(key);
		const auto index = json[key].toInt();

		Q_ASSERT(list != nullptr);
		Q_ASSERT(index != -1);

		list->setCurrentIndex(index);
	}

	if (json.contains(Database::TERMINAL_SERVER)) {
		const auto server = json[Database::TERMINAL_SERVER].toString();
		const auto index = ui_->server->findText(server);
		Q_ASSERT(index != -1);

		ui_->server->setCurrentIndex(index);
	}
}

void TerminalEdit::save(QJsonObject & json)
{
	for (const auto & key : EDIT_KEYS) {
		auto edit = findChild<QLineEdit*>(key);
		Q_ASSERT(edit != nullptr);

		json[key] = edit->text();
	}

	for (const auto & key : LIST_KEYS) {
		auto list = findChild<QComboBox*>(key);
		Q_ASSERT(list != nullptr);

		json[key] = list->currentIndex();
	}

	if (ui_->server->currentIndex() > 0) {
		json[Database::TERMINAL_SERVER] = ui_->server->currentText();
	} else {
		json.remove(Database::TERMINAL_SERVER);
	}

	if (!json.contains(Database::TERMINAL_UUID)) {
		const auto uuid = QUuid::createUuid().toString();
		json[Database::TERMINAL_UUID] = uuid.mid(1, uuid.size() - 2);
	}

	const auto uuid = json[Database::TERMINAL_UUID].toString();
	auto dir = QFileInfo(Config::database()).dir();
	const QFileInfo source(json[Database::TERMINAL_SRVFILE].toString());

	if (!dir.cd(uuid)) {
		if (!dir.exists(uuid)) {
			dir.mkdir(uuid);
		} else if (!QFileInfo(dir.absoluteFilePath(uuid)).isDir()) {
			dir.remove(uuid);
			dir.mkdir(uuid);
		}
		const auto ok = dir.cd(uuid);
		Q_ASSERT(ok);
	}

	const auto destination = dir.absoluteFilePath(source.fileName());
	if (dir.exists(source.fileName())) {
		if (source.absoluteFilePath() == destination) {
			return;
		}
		dir.remove(source.fileName());
	}

	if (QFile::copy(source.absoluteFilePath(), destination)) {
		json[Database::TERMINAL_SRVFILE] = destination;
	}
}

QStringList TerminalEdit::balanceNames()
{
	QStringList result;
	TerminalEdit me;
	const auto count = me.ui_->balance->count() - 1;

	result.reserve(count + 1);
	for (auto i = 0; i < count; ++i) {
		result.append(me.ui_->balance->itemText(i));
	}
	result.append(QString::null);

	return result;
}

void TerminalEdit::changeEvent(QEvent * e)
{
	if (e->type() == QEvent::LanguageChange) {
		ui_->retranslateUi(this);
	} else {
		QDialog::changeEvent(e);
	}
}

void TerminalEdit::accept()
{
	for (const auto & key : EDIT_KEYS) {
		auto edit = findChild<QLineEdit*>(key);
		Q_ASSERT(edit != nullptr);
		if (edit->text().isEmpty()) {
			edit->setFocus();
			return;
		}
	}

	auto dir = QFileInfo(Config::database()).dir();
	const auto edit = findChild<QLineEdit*>(Database::TERMINAL_SRVFILE);
	const QFileInfo source(edit->text());

	if (!source.exists() || !source.isFile()) {
		edit->setFocus();
		return;
	}

	QDialog::accept();
}

void TerminalEdit::on_buttonFile_clicked()
{
	QFileDialog dialog(this);

	dialog.setFilter(QDir::Files);
	dialog.setFileMode(QFileDialog::ExistingFile);
	dialog.setAcceptMode(QFileDialog::AcceptOpen);
	dialog.setNameFilter(SRV_FILTER);

	if (dialog.exec() != 0) {
		Q_ASSERT(!dialog.selectedFiles().isEmpty());
		
		const auto edit = findChild<QLineEdit*>(Database::TERMINAL_SRVFILE);
		Q_ASSERT(edit != nullptr);
		
		edit->setText(dialog.selectedFiles().first());
	}
}