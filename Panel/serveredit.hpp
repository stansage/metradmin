#pragma once

namespace Ui {
	class ServerEdit;
}

class ServerEdit : public QDialog
{
	Q_OBJECT

public:
	explicit ServerEdit(QWidget * parent = nullptr);
	~ServerEdit() override;

	void load(const QJsonObject & json);
	void save(QJsonObject & json);

protected:
	void changeEvent(QEvent * e);

public slots:
	void accept() override;

private:
	Ui::ServerEdit * ui_;
};

