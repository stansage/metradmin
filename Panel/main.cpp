#include "stdafx.h"
#include "mainwindow.hpp"
#include "config.hpp"

namespace
{
	static const QString CONFIG_SUFFIX = QStringLiteral(".config");

	void messageHandler(QtMsgType type, const QMessageLogContext & context, const QString & msg)
	{
		static std::ofstream logger(QDir(QString::fromLocal8Bit(getenv("TEMP"))).absoluteFilePath(QUuid::createUuid().toString()).toLocal8Bit().toStdString());
		
		Q_ASSERT(logger.is_open());

		switch (type) {
		case QtDebugMsg:
			logger << "Debug";
			break;
		case QtInfoMsg:
			logger << "Info";
			break;
		case QtWarningMsg:
			logger << "Warning";
			break;
		case QtCriticalMsg:
			logger << "Critical";
			break;
		case QtFatalMsg:
			logger << "Fatal";
			break;
		}

		logger << ": " << msg.toLocal8Bit().toStdString() << std::endl;

		if (type == QtFatalMsg) {
			abort();
		}
	}
}

int main(int argc, char *argv[])
{
#ifdef QT_NO_DEBUG
	qInstallMessageHandler(messageHandler);
#endif

	QApplication a(argc, argv);
	a.setApplicationName(QFileInfo(a.applicationFilePath()).baseName());

	const auto config = QDir(a.applicationDirPath()).absoluteFilePath(a.applicationName() + CONFIG_SUFFIX);
	if (Config::instance().open(config)) {
		MainWindow w;
		w.show();
		return a.exec();
	} 
	
	return 2;
}

