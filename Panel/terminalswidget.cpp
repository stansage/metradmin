#include "stdafx.h"
#include "terminalswidget.hpp"
#include "ui_terminalswidget.h"
#include "mainwindow.hpp"
#include "database.hpp"
#include "terminaledit.hpp"

TerminalsWidget::TerminalsWidget(QWidget * parent) :
    QWidget(parent),
    ui_(new Ui::TerminalsWidget)
{
    ui_->setupUi(this);

	ui_->buttonAdd->setIcon(MainWindow::getIcon(MainWindow::Icon::NewDir));
	ui_->buttonRestart->setIcon(MainWindow::getIcon(MainWindow::Icon::Refresh));


	Q_ASSERT(ui_->table->columnCount() == 6);
	Q_ASSERT(ui_->table->horizontalHeader()->count() == 6);
	ui_->table->setHorizontalHeaderItem(5, new QTableWidgetItem());

	QTimer::singleShot(0, [this]() {
		const auto database = MainWindow::getInstance(this)->database();
		for (const auto entry : database->terminals()) {
			Q_ASSERT(entry.isObject());

			addTerminal(entry.toObject());
		}
	});
}

TerminalsWidget::~TerminalsWidget()
{
    delete ui_;
}

void TerminalsWidget::changeEvent(QEvent * e)
{
	if (e->type() == QEvent::LanguageChange) {
		ui_->retranslateUi(this);
	} else {
		QWidget::changeEvent(e);
	}
}

void TerminalsWidget::addTerminal(const QJsonObject & terminal)
{
	const auto row = ui_->table->rowCount();
	const auto name = new QTableWidgetItem(terminal[Database::TERMINAL_NAME].toString());
	const auto server = new QTableWidgetItem();
	const auto balance = new QTableWidgetItem();
	const auto currency = new QTableWidgetItem();
	const auto uuid = terminal[Database::TERMINAL_UUID].toString();
	
	auto icon = MainWindow::getIcon(MainWindow::Icon::StatusGray);

	name->setData(Qt::UserRole, uuid);
	balance->setData(Qt::UserRole, terminal[Database::TERMINAL_UUID].toInt());

	if (terminal.contains(Database::TERMINAL_SERVER)) {
		icon = MainWindow::getIcon(MainWindow::Icon::StatusRed);
		server->setText(terminal[Database::TERMINAL_SERVER].toString());
	}

	ui_->table->setRowCount(row + 1);
	ui_->table->setItem(row, 1, server);
	ui_->table->setItem(row, 2, name);
	ui_->table->setItem(row, 3, balance);
	ui_->table->setItem(row, 4, currency);
	ui_->table->setCellIcon(row, 0, icon);
	ui_->table->setCellButtons(row, 5, TableWidget::AllButtons);
}

void TerminalsWidget::onServerRemoved(QString server)
{
	for (auto row = ui_->table->rowCount() - 1; row >= 0; --row) {
		const auto item = ui_->table->item(row, 1);

		if (item->text() == server) {
			const auto uuid = ui_->table->item(row, 2)->data(Qt::UserRole).toString();
			const auto database = MainWindow::getInstance(this)->database();
			const auto index = database->findTerminal(uuid);
			const auto icon = MainWindow::getIcon(MainWindow::Icon::StatusGray);
			auto terminals = database->terminals();

			Q_ASSERT(index != -1);
			
			auto terminal = terminals[index].toObject();

			terminal.remove(Database::TERMINAL_SERVER);
			terminals.replace(index, terminal);
			database->setTerminals(terminals);
			database->commit();

			item->setText(QString::null);
			ui_->table->setCellIcon(row, 0, icon);
		}
	}
}

void TerminalsWidget::onServerChanged(QString previous, QString current)
{
	for (auto row = ui_->table->rowCount() - 1; row >= 0; --row) {
		const auto item = ui_->table->item(row, 1);

		if (item->text() == previous) {
			const auto uuid = ui_->table->item(row, 2)->data(Qt::UserRole).toString();
			const auto database = MainWindow::getInstance(this)->database();
			const auto index = database->findTerminal(uuid);
			auto terminals = database->terminals();

			Q_ASSERT(index != -1);

			auto terminal = terminals[index].toObject();

			terminal[Database::TERMINAL_SERVER] = current;
			terminals.replace(index, terminal);
			database->setTerminals(terminals);
			database->commit();

			item->setText(current);
		}
	}
}

void TerminalsWidget::raise(QString uuid, QString balance, QString currency)
{
	static const auto NAMES = TerminalEdit::balanceNames();

	QHash<int, QHash<QString, double> > total;
	for (auto row = ui_->table->rowCount() - 1; row >= 0; --row) {
		const auto itemName = ui_->table->item(row, 2);
		const auto itemBalance = ui_->table->item(row, 3);
		const auto itemCurrency = ui_->table->item(row, 4);

		if (itemName->data(Qt::UserRole).toString() == uuid) {
			itemBalance->setText(balance);
			itemCurrency->setText(currency);
			ui_->table->setCellIcon(row, 0, MainWindow::getIcon(MainWindow::Icon::StatusGreen));
		}
		total[itemBalance->data(Qt::UserRole).toInt()][currency] += balance.toDouble();
	}

	ui_->balance->setRowCount(0);

	Q_ASSERT(total.size() <= NAMES.size());
	for (auto it1 = total.constBegin(), end1 = total.constEnd(); it1 != end1; ++it1) {
		Q_ASSERT(it1.key() <= NAMES.size());

		for (auto it2 = it1.value().constBegin(), end2 = it1.value().constEnd(); it2 != end2; ++it2) {
			const auto row = ui_->balance->rowCount();
			ui_->balance->insertRow(row);
			ui_->balance->setItem(row, 0, new QTableWidgetItem(NAMES[it1.key()]));
			ui_->balance->setItem(row, 1, new QTableWidgetItem(it2.key()));
			ui_->balance->setItem(row, 2, new QTableWidgetItem(QString::number(it2.value())));
		}
	}
}

void TerminalsWidget::fall(QString uuid)
{
	for (auto row = ui_->table->rowCount() - 1; row >= 0; --row) {
		const auto item = ui_->table->item(row, 2);
		if (item->data(Qt::UserRole).toString() == uuid) {
			ui_->table->setCellIcon(row, 0, MainWindow::getIcon(MainWindow::Icon::StatusRed));
		}
	}
}

void TerminalsWidget::on_actionConfirm_triggered()
{
	const auto action = qobject_cast<QAction*>(sender());
	const auto row = action->data().toInt();
	const auto item = ui_->table->item(row, 2);

	if (QMessageBox::Yes == QMessageBox::question(this, action->toolTip(), action->text().arg(item->text()))) {
		const auto database = MainWindow::getInstance(this)->database();
		const auto uuid = item->data(Qt::UserRole).toString();
		const auto index = database->findTerminal(uuid);
		auto terminals = database->terminals();

		Q_ASSERT(!uuid.isEmpty());
		Q_ASSERT(index != -1);

		terminals.removeAt(index);
		database->setTerminals(terminals);
		database->commit();

		ui_->table->removeRow(row);
	}
}


void TerminalsWidget::on_buttonAdd_clicked()
{
	TerminalEdit dialog(this);
	if (dialog.exec() != QDialog::Accepted) {
		return;
	};

	QJsonObject terminal;
	dialog.save(terminal);

	const auto database = MainWindow::getInstance(this)->database();
	auto terminals = database->terminals();

	terminals.append(terminal);
	database->setTerminals(terminals);
	database->commit();

	addTerminal(terminal);
}

void TerminalsWidget::on_table_edit(int row)
{
	const auto server = ui_->table->item(row, 1);
	const auto name = ui_->table->item(row, 2);
	const auto balance = ui_->table->item(row, 3);
	const auto database = MainWindow::getInstance(this)->database();
	const auto index = database->findTerminal(name->data(Qt::UserRole).toString());
	auto icon = MainWindow::getIcon(MainWindow::Icon::StatusGray);

	Q_ASSERT(index != -1);

	TerminalEdit dialog(this);
	auto terminals = database->terminals();
	auto terminal = terminals[index].toObject();
	
	dialog.load(terminal);
	if (dialog.exec() != QDialog::Accepted) {
		return;
	}

	dialog.save(terminal);
	terminals.replace(index, terminal);
	database->setTerminals(terminals);
	database->commit();

	if (terminal.contains(Database::TERMINAL_SERVER)) {
		icon = MainWindow::getIcon(MainWindow::Icon::StatusRed);
		server->setText(terminal[Database::TERMINAL_SERVER].toString());
	} else {
		server->setText(QString::null);
	}

	name->setText(terminal[Database::TERMINAL_NAME].toString());
	balance->setData(Qt::UserRole, terminal[Database::TERMINAL_BALANCE].toInt());
	ui_->table->setCellIcon(row, 0, icon);
}

void TerminalsWidget::on_table_remove(int row)
{
	ui_->actionConfirm->setData(row);
	ui_->actionConfirm->trigger();
}

void TerminalsWidget::on_table_restart(int row)
{
	emit restarted(ui_->table->item(row, 2)->data(Qt::UserRole).toString());
}

void TerminalsWidget::on_buttonRestart_clicked()
{
	emit restarted(QString::null);
}