#include "stdafx.h"
#include "config.hpp"

namespace
{
	static const QString KEY_DATABASE = QStringLiteral("database");
	static const QString KEY_TIMEOUT = QStringLiteral("timeout");
	static const QString KEY_LANGUAGE = QStringLiteral("language");
	static const QString KEY_STYLE = QStringLiteral("style");
	static const QString KEY_SERVERS = QStringLiteral("servers");
	static const QString KEY_TERMINALS = QStringLiteral("terminals");
	static const QString VALUE_DATABASE = KEY_DATABASE + QStringLiteral(".json");
	static const auto VALUE_TIMEOUT = 1000;
	static const auto VALUE_LANGUAGE = 0;
}

struct Config::Impl
{
	QFile file;
	QJsonObject root;
};

Config::Config() :
impl_(new Impl)
{
}


Config::~Config()
{
	delete impl_;
}

Config & Config::instance()
{
	static Config config;
	return config;
}

bool Config::open(const QString & path)
{
	impl_->file.setFileName(path);

	if (!impl_->file.exists()) {
		qWarning() << "No config found at" << path;
		impl_->root[KEY_DATABASE] = QFileInfo(path).dir().absoluteFilePath(VALUE_DATABASE);
		return true;
	}

	if (!impl_->file.open(QFile::ReadOnly)) {
		qCritical() << "Failed to open config" << path << impl_->file.errorString();
	} else {
		QJsonParseError error;
		const auto json = QJsonDocument::fromJson(impl_->file.readAll(), &error);
		if (error.error == QJsonParseError::NoError && json.isObject()) {
			impl_->root = json.object();
			if (!impl_->root.contains(KEY_DATABASE)) {
				impl_->root[KEY_DATABASE] = QFileInfo(path).dir().absoluteFilePath(VALUE_DATABASE);
			}
			impl_->file.close();
			return true;
		}

		if (error.error != QJsonParseError::NoError) {
			qCritical() << "Failed to parse config" << error.errorString() << error.offset;
		} else {
			qCritical() << "Invalid config" << json.toJson();
		}
	}

	return false;
}

void Config::save(const std::tuple<QString, int, int, QString> & values)
{
	instance().impl_->root[KEY_DATABASE] = std::get<0>(values);
	instance().impl_->root[KEY_TIMEOUT] = std::get<1>(values);
	instance().impl_->root[KEY_LANGUAGE] = std::get<2>(values);
	instance().impl_->root[KEY_STYLE] = std::get<3>(values);

	if (!impl_->file.open(QFile::WriteOnly)) {
		qCritical() << "Failed to write config" << impl_->file.fileName() << impl_->file.errorString();
	} else {
		QJsonDocument json(instance().impl_->root);
		impl_->file.write(json.toJson());
		impl_->file.close();
	}
}

QString Config::database()
{
	return instance().impl_->root[KEY_DATABASE].toString();

}

int Config::timeout()
{
	return instance().impl_->root[KEY_TIMEOUT].toInt(VALUE_TIMEOUT);
}

int Config::language()
{
	return instance().impl_->root[KEY_LANGUAGE].toInt(VALUE_LANGUAGE);
}

QString Config::style()
{
	return instance().impl_->root[KEY_STYLE].toString();

}
