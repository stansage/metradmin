#pragma once

class TableWidget : public QTableWidget
{
	Q_OBJECT

public:
	explicit TableWidget(QWidget * parent = nullptr);

	enum Button
	{
		ButtonEdit = 1,
		ButtonRemove = 2,
		ButtonRestart = 4,
		AllButtons = 7
	};
	Q_DECLARE_FLAGS(Buttons, Button)

	void setCellIcon(int row, int column, const QIcon & icon);
	void setCellButtons(int row, int column, Buttons buttons);

signals:
	void edit(int row);
	void remove(int row);
	void restart(int row);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(TableWidget::Buttons)
