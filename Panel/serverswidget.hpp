#pragma once

namespace Ui {
class ServersWidget;
}

class ServersWidget : public QWidget
{
	Q_OBJECT

public:
	explicit ServersWidget(QWidget * parent = nullptr);
	~ServersWidget() override;

protected:
	void changeEvent(QEvent * e);

private:
	void addServer(const QString & name);

signals:
	void removed(QString server);
	void changed(QString previous, QString current);

public slots:
	void onConnected(QString server);
	void onDisconnected(QString server);

private slots:
	void on_actionExists_triggered();
	void on_actionConfirm_triggered();

	void on_buttonAdd_clicked();
	void on_table_edit(int row);
	void on_table_remove(int row);
	

private:
	Ui::ServersWidget * ui_;
};

