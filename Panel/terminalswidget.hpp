#pragma once

namespace Ui {
class TerminalsWidget;
}

class TerminalsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit TerminalsWidget(QWidget * parent = nullptr);
	~TerminalsWidget() override;

protected:
	void changeEvent(QEvent * e);

private:
	void addTerminal(const QJsonObject & terminal);

signals:
	void restarted(QString uuid);

public slots:
	void onServerRemoved(QString server);
	void onServerChanged(QString previous, QString current);
	void raise(QString uuid, QString balance, QString currency);
	void fall(QString uuid);

private slots:
	void on_actionConfirm_triggered();
	void on_buttonAdd_clicked();
	void on_table_edit(int row);
	void on_table_remove(int row);
	void on_table_restart(int row);
	void on_buttonRestart_clicked();

private:
	Ui::TerminalsWidget * ui_;
};

