#pragma once

class Database;

class Transport : public QObject
{
	Q_OBJECT

public:
	explicit Transport(Database * database);

private:
	Database * db() const;
	void reconnect(QTcpSocket * socket);
	void transfer();

signals:
	void connected(QString server);
	void disconnected(QString server);
	void raised(QString uuid, QString balance, QString currency);
	void fell(QString uuid);

public slots:
	void update();
	void restart(QString filter);

private slots:
	void onError(QAbstractSocket::SocketError error);
	void onConnected();
	void onDisconnected();
	void onReadyRead();
};

