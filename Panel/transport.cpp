#include "stdafx.h"
#include "transport.hpp"
#include "config.hpp"
#include "database.hpp"

Transport::Transport(Database * database) :
QObject(database)
{
	connect(database, &Database::updated, this, &Transport::update);
}

Database * Transport::db() const
{
	const auto result = qobject_cast<Database*>(parent());
	Q_ASSERT(result != nullptr);
	return result;
}

void Transport::reconnect(QTcpSocket * socket)
{
	Q_ASSERT(socket != nullptr);
	const auto index = db()->findServer(socket->objectName());
	Q_ASSERT(index != -1);
	const auto server = db()->servers()[index].toObject();
	const auto ip = server[Database::SERVER_IP].toString();
	const auto port = server[Database::SERVER_PORT].toInt();

	socket->connectToHost(QHostAddress(ip), port);
}

void Transport::transfer()
{
	static const auto KEYS = {
		Database::TERMINAL_UUID,
//		Database::TERMINAL_TYPE,
		Database::TERMINAL_LOGIN,
		Database::TERMINAL_PASSWORD,
		Database::TERMINAL_SYMBOL
	};

	for (const auto entry : db()->terminals()) {
		const auto terminal = entry.toObject();
		if (!terminal.contains(Database::TERMINAL_SERVER)) {
			continue;
		}

		const auto server = terminal[Database::TERMINAL_SERVER].toString();
		const auto socket = findChild<QTcpSocket*>(server);
		if (socket == nullptr || !socket->isOpen()) {
			continue;
		}

		QFile srvfile(terminal[Database::TERMINAL_SRVFILE].toString());
		if (!srvfile.open(QFile::ReadOnly)) {
			qWarning() << "Failed to read srv-file" << srvfile.fileName() << srvfile.errorString();
			continue;
		}

		QJsonObject packet;
		packet[Database::TERMINAL_SRVFILE] = QFileInfo(srvfile.fileName()).fileName();
		for (const auto & key : KEYS) {
			packet[key] = terminal[key];
		}

		auto data = QJsonDocument(packet).toJson();
		data.append('\x04');
		socket->write(data);

		data = srvfile.readAll();
		socket->write(QByteArray::fromStdString(std::to_string(data.size())));
		data.push_front('\x00');
		socket->write(data);
	}
}

void Transport::update()
{
	for (const auto entry : db()->servers()) {
		const auto server = entry.toObject();
		const auto name = server[Database::SERVER_NAME].toString();
		const auto ip = server[Database::SERVER_IP].toString();
		const auto port = server[Database::SERVER_PORT].toInt();

		auto socket = findChild<QTcpSocket*>(name);
		if (socket == nullptr) {
			socket = new QTcpSocket(this);
			socket->setObjectName(name);
			connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), SLOT(onError(QAbstractSocket::SocketError)));
			connect(socket, &QTcpSocket::connected, this, &Transport::onConnected);
			connect(socket, &QTcpSocket::disconnected, this, &Transport::onDisconnected);
			connect(socket, &QTcpSocket::disconnected, this, &Transport::onDisconnected);
			connect(socket, &QTcpSocket::readyRead, this, &Transport::onReadyRead);
			socket->connectToHost(QHostAddress(ip), port);
		} else if (socket->peerAddress().toString() != ip || socket->peerPort() != port) {
			socket->close();
		}
	}
	transfer();
}

void Transport::restart(QString filter)
{
	for (const auto entry : db()->terminals()) {
		const auto terminal = entry.toObject();
		const auto uuid = terminal[Database::TERMINAL_UUID].toString();

		if (!filter.isEmpty() && filter != uuid) {
			continue;
		}

		if (!terminal.contains(Database::TERMINAL_SERVER)) {
			continue;
		}

		const auto server = terminal[Database::TERMINAL_SERVER].toString();
		const auto socket = findChild<QTcpSocket*>(server);
		if (socket == nullptr || !socket->isOpen()) {
			continue;
		}
	
		auto data = uuid.toLatin1();
		data.push_front('#');
		data.push_back('\x04');
		socket->write(data);
	}
}

void Transport::onError(QAbstractSocket::SocketError error)
{
	const auto socket = qobject_cast<QTcpSocket*>(sender());
	Q_ASSERT(socket != nullptr);

	QTimer::singleShot(Config::timeout(), std::bind(&Transport::reconnect, this, socket));
}

void Transport::onConnected()
{
	const auto socket = qobject_cast<QTcpSocket*>(sender());
	Q_ASSERT(socket != nullptr);
	
	emit connected(socket->objectName());
	transfer();
}

void Transport::onDisconnected()
{
	const auto socket = qobject_cast<QTcpSocket*>(sender());
	Q_ASSERT(socket != nullptr);

	emit disconnected(socket->objectName());
	reconnect(socket);
}

void Transport::onReadyRead()
{
	const auto socket = qobject_cast<QTcpSocket*>(sender());
	Q_ASSERT(socket != nullptr);
	QJsonParseError error;
	const auto data = socket->readAll();
	const auto json = QJsonDocument::fromJson(data, &error);
	if (error.error != QJsonParseError::NoError) {
		qWarning() << data << error.errorString() << error.offset;
	} else {
		Q_ASSERT(json.isObject());
		const auto entry = json.object();
		const auto uuid = entry[Database::TERMINAL_UUID].toString();
		
		if (!entry.contains(Database::TERMINAL_CURRENCY))  {
			emit fell(uuid);
		} else {
			Q_ASSERT(entry.contains(Database::TERMINAL_BALANCE));
			const auto balance = entry[Database::TERMINAL_BALANCE].toString();
			const auto currency = entry[Database::TERMINAL_CURRENCY].toString();
			
			emit raised(uuid, balance, currency);
		}
	}
}