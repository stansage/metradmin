#include "stdafx.h"
#include "settingswidget.hpp"
#include "ui_settingswidget.h"
#include "config.hpp"
#include "mainwindow.hpp"

namespace
{
	static const QString TRANSLATION_RU = QStringLiteral(":/panel_ru");
	static const QString DATABASE_FILTER = QStringLiteral("JSON (*.json)");
}

SettingsWidget::SettingsWidget(QWidget * parent) :
    QWidget(parent),
    ui_(new Ui::SettingsWidget)
{
    ui_->setupUi(this);

	ui_->buttonApply->setIcon(MainWindow::getIcon(MainWindow::Icon::Apply));
	ui_->buttonReset->setIcon(MainWindow::getIcon(MainWindow::Icon::Reset));

	auto translator = new QTranslator(this);
	translator->load(TRANSLATION_RU);

	QTimer::singleShot(0, std::bind(&QPushButton::click, ui_->buttonReset));
}

SettingsWidget::~SettingsWidget()
{
    delete ui_;
}

void SettingsWidget::changeEvent(QEvent * e)
{
	if (e->type() == QEvent::LanguageChange) {
		const auto language = ui_->languages->currentIndex();
		ui_->retranslateUi(this);
		ui_->languages->setCurrentIndex(language);
	} else {
		QWidget::changeEvent(e);
	}
}

void SettingsWidget::changeLanguage()
{
	switch (ui_->languages->currentIndex()) {
	case 0:
		if (translator_ != nullptr) {
			QApplication::removeTranslator(translator_);
			translator_ = nullptr;
		}
		break;
	case 1:
		if (translator_ == nullptr) {
			translator_ = findChild<QTranslator*>();
			QApplication::installTranslator(translator_);
		}
		break;

	default:
		Q_ASSERT(ui_->languages->currentIndex() < 2);
	}
}

void SettingsWidget::changeStyle()
{
	MainWindow::getInstance(this)->setStyleSheet(ui_->style->toPlainText());

}

void SettingsWidget::on_buttonApply_clicked()
{
	const auto values = std::make_tuple(ui_->database->text(),
	                                    ui_->timeout->value(),
										ui_->languages->currentIndex(),
										ui_->style->toPlainText());
	Config::instance().save(values);
	changeLanguage();
	changeStyle();
}

void SettingsWidget::on_buttonReset_clicked()
{
	ui_->database->setText(Config::database());
	ui_->languages->setCurrentIndex(Config::language());
	ui_->timeout->setValue(Config::timeout()),
	ui_->style->setPlainText(Config::style());
	changeLanguage();
	changeStyle();
}

void SettingsWidget::on_buttonDatabase_clicked()
{
	QFileDialog dialog(this);
	QFileInfo info(ui_->database->text());
	
	dialog.setFilter(QDir::Files);
	dialog.setFileMode(QFileDialog::ExistingFile);
	dialog.setAcceptMode(QFileDialog::AcceptOpen);
	dialog.setNameFilter(DATABASE_FILTER);
	dialog.setDirectory(info.dir());
	dialog.selectFile(info.fileName());
	
	if (dialog.exec() != 0) {
		Q_ASSERT(!dialog.selectedFiles().isEmpty());
		ui_->database->setText(dialog.selectedFiles().front());
	}
}

