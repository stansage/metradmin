<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MeTrAdmin</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="25"/>
        <source>Trade</source>
        <translation>Торговля</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Terminals</source>
        <translation>Терминалы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="35"/>
        <source>Servers</source>
        <translation>Серверы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>ServerEdit</name>
    <message>
        <location filename="serveredit.ui" line="14"/>
        <source>Server</source>
        <translation>Сервер</translation>
    </message>
    <message>
        <location filename="serveredit.ui" line="23"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="serveredit.ui" line="33"/>
        <source>Required (unique)</source>
        <translation>Обязательное(уникальное)</translation>
    </message>
    <message>
        <location filename="serveredit.ui" line="40"/>
        <source>IP:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="serveredit.ui" line="50"/>
        <location filename="serveredit.ui" line="70"/>
        <source>Required</source>
        <translation>Обязательное</translation>
    </message>
    <message>
        <location filename="serveredit.ui" line="57"/>
        <source>Port:</source>
        <translation>Порт:</translation>
    </message>
    <message>
        <location filename="serveredit.ui" line="67"/>
        <source>7007</source>
        <translation>7007</translation>
    </message>
    <message>
        <location filename="serveredit.ui" line="92"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="serveredit.ui" line="108"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>ServersWidget</name>
    <message>
        <location filename="serverswidget.ui" line="19"/>
        <source>Add server</source>
        <oldsource>Add Server</oldsource>
        <translation>Добавить сервер</translation>
    </message>
    <message>
        <location filename="serverswidget.ui" line="75"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="serverswidget.ui" line="80"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="serverswidget.ui" line="89"/>
        <source>Server &apos;%1&apos; already exists!</source>
        <translation>Сервер &apos;%1&apos; уже существует!</translation>
    </message>
    <message>
        <location filename="serverswidget.ui" line="92"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="serverswidget.ui" line="97"/>
        <source>Are you sure to remove server &apos;%1&apos;?</source>
        <translation>Вы действительно хотите удалить сервер &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="serverswidget.ui" line="100"/>
        <source>Confirmation</source>
        <translation>Подтверждение</translation>
    </message>
</context>
<context>
    <name>SettingsWidget</name>
    <message>
        <location filename="settingswidget.ui" line="17"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="28"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="33"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="41"/>
        <source>Style:</source>
        <translation>Стиль:</translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="69"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="82"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="97"/>
        <source>Database:</source>
        <translation>База данных:</translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="112"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settingswidget.ui" line="121"/>
        <source>Timeout, ms</source>
        <translation>Задержка, мс</translation>
    </message>
</context>
<context>
    <name>TerminalEdit</name>
    <message>
        <location filename="terminaledit.ui" line="42"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="52"/>
        <location filename="terminaledit.ui" line="117"/>
        <location filename="terminaledit.ui" line="134"/>
        <location filename="terminaledit.ui" line="151"/>
        <location filename="terminaledit.ui" line="170"/>
        <source>Required</source>
        <translation>Обязательное</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="59"/>
        <source>Type:</source>
        <translation>Тип:</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="70"/>
        <source>MT4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="78"/>
        <source>Balance:</source>
        <translation>Баланс:</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="89"/>
        <source>Balance1</source>
        <translation>Баланс1</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="94"/>
        <source>Balance2</source>
        <translation>Баланс2</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="34"/>
        <location filename="terminaledit.ui" line="99"/>
        <source>None</source>
        <translation>Не задан</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="14"/>
        <source>Terminal</source>
        <translation>Терминал</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="107"/>
        <source>Login:</source>
        <translation>Логин:</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="124"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="201"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="214"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="141"/>
        <source>Symbol:</source>
        <oldsource>Symbol</oldsource>
        <translation>Символ:</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="158"/>
        <source>SRV-file:</source>
        <oldsource>SRV-file</oldsource>
        <translation>SRV-файл:</translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="177"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="terminaledit.ui" line="23"/>
        <source>Server:</source>
        <translation>Сервер:</translation>
    </message>
</context>
<context>
    <name>TerminalsWidget</name>
    <message>
        <location filename="terminalswidget.ui" line="19"/>
        <source>Add terminal</source>
        <oldsource>Add Terminal</oldsource>
        <translation>Добавить терминал</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="32"/>
        <source>Restart all</source>
        <oldsource>Restart</oldsource>
        <translation>Перезапустить все</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="85"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="90"/>
        <source>Server</source>
        <translation>Сервер</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="95"/>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="100"/>
        <source>Balance</source>
        <translation>Баланс</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="105"/>
        <location filename="terminalswidget.ui" line="145"/>
        <source>Currency</source>
        <translation>Валюта</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="140"/>
        <source>Total</source>
        <translation>Итого</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="150"/>
        <source>Sum</source>
        <translation>Сумма</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="158"/>
        <source>Are you sure to remove terminal &apos;%1&apos;?</source>
        <translation>Вы действительно хотите удалить терминал &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="terminalswidget.ui" line="161"/>
        <source>Confirmation</source>
        <translation>Подтверждение</translation>
    </message>
</context>
</TS>
