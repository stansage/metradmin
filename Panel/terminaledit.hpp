#pragma once

namespace Ui {
	class TerminalEdit;
}

class TerminalEdit : public QDialog
{
	Q_OBJECT

public:
	explicit TerminalEdit(QWidget * parent = nullptr);
	~TerminalEdit() override;

	void load(const QJsonObject & json);
	void save(QJsonObject & json);

	static QStringList balanceNames();
	
protected:
	void changeEvent(QEvent * e);

public slots:
	void accept() override;

private slots:
	void on_buttonFile_clicked();

private:
	Ui::TerminalEdit * ui_;
};

