#pragma once

namespace Ui {
	class MainWindow;
}

class Database;
class Transport;

class MainWindow : public QMainWindow
{
    Q_OBJECT
	Q_PROPERTY(Database* database READ database)

public:
    explicit MainWindow(QWidget * parent = nullptr);
    ~MainWindow() override;

	Database * database() const;

	enum class Icon { 
		NewDir,
		NetDrive,
		Save,
		Apply,
		Reset,
		Cancel,
		Refresh,
		Edit,
		Close,
		StatusRed,
		StatusGreen,
		StatusGray
	};
	static QIcon getIcon(Icon icon);

	static MainWindow * getInstance(QWidget * widget);

protected:
	void changeEvent(QEvent * e);
	
/*
private slots:
	void on_actionNew_triggered();
	void on_actionSave_triggered();
    void on_actionQuit_triggered();

private:
	const QString SETTINGS_COLUMNS = QStringLiteral("columns");
	const QString SETTINGS_ITEMS = QStringLiteral("items");
*/
    Ui::MainWindow * ui_;
	Database * db_;
	Transport * transport_;
};

