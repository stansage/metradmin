#include "stdafx.h"
#include "tablewidget.hpp"
#include "mainwindow.hpp"

namespace 
{
	static const auto BUTTON_ICONS = { 
		MainWindow::Icon::Refresh, 
		MainWindow::Icon::Edit, 
		MainWindow::Icon::Close 
	};
}

TableWidget::TableWidget(QWidget * parent) :
QTableWidget(parent)
{

}

void TableWidget::setCellIcon(int row, int column, const QIcon & icon)
{
	const auto widget = new QWidget();
	const auto button = new QPushButton(icon, QString::null);

	button->setFlat(true);
	widget->setLayout(new QHBoxLayout());
	widget->layout()->setContentsMargins(0, 0, 0, 0);
	widget->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));
	widget->layout()->addWidget(button);
	widget->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));

	setCellWidget(row, column, widget);
}

void TableWidget::setCellButtons(int row, int column, Buttons buttons)
{
	const auto widget = new QWidget();
	
	widget->setMinimumHeight(32);
	widget->setLayout(new QHBoxLayout());
	widget->layout()->setContentsMargins(0, 0, 0, 0);
	widget->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding));

	for (const auto icon : BUTTON_ICONS) {
		switch (icon) {
		case MainWindow::Icon::Edit:
			if (!buttons.testFlag(ButtonEdit)) {
				continue;
			}
			break;
		case MainWindow::Icon::Close:
			if (!buttons.testFlag(ButtonRemove)) {
				continue;
			}
			break;
		case MainWindow::Icon::Refresh:
			if (!buttons.testFlag(ButtonRestart)) {
				continue;
			}
			break;
		}

		const auto button = new QPushButton(MainWindow::getIcon(icon), QString::null);
		button->setIconSize(QSize(widget->minimumHeight(), widget->minimumHeight()));
		connect(button, &QPushButton::clicked, [this, icon, row]() {
			switch (icon) {
			case MainWindow::Icon::Edit:
				emit edit(row);
				break;
			case MainWindow::Icon::Close:
				emit remove(row);
				break;
			case MainWindow::Icon::Refresh:
				emit restart(row);
				break;
			}
		});
		widget->layout()->addWidget(button);
	}
	
	setCellWidget(row, column, widget);
}
