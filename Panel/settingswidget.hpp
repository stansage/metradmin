#pragma once

namespace Ui {
class SettingsWidget;
}

class SettingsWidget : public QWidget
{
	Q_OBJECT

public:
	explicit SettingsWidget(QWidget * parent = nullptr);
	~SettingsWidget() override;

protected:
	void changeEvent(QEvent * e);

private:
	void changeLanguage();
	void changeStyle();

private slots:
	void on_buttonApply_clicked();
	void on_buttonReset_clicked();
	void on_buttonDatabase_clicked();

private:
	Ui::SettingsWidget * ui_;
	QTranslator * translator_ = nullptr;
};

